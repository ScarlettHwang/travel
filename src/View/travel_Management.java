package View;

import Bean.Tourism_Group;
import Bean.Tourism_Line;
import Bean.Tourism_information;
import Controller.Select;
import Controller.Updata;
import Picture.Background;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

//实现删除功能
//提供修改时，获取被选择的信息数据
//全部路线详情界面表格样式
public class travel_Management extends JFrame  {

	Select select = new Select();
	Updata updata = new Updata();
	JTable jTable;
	DefaultTableModel dt;

	Object[] header= {"路线号","旅游团号","费用","详情介绍","旅游团名","导游姓名","地址","电话","出发地","目的地","天数","主要景点"};
	Object[][] data=select.getTouristinfo("select * from travel_information as t left join tourism_group as tg using (group_num) left join tourism_line as tl using (route_num)");

	public travel_Management() {
		super("路线信息管理");
		this.setBounds(0, 0, 1550, 740);
		this.setExtendedState(JFrame.MAXIMIZED_HORIZ);
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示

		//this.setResizable(false);//让窗口大小不可改变
		getContentPane().setLayout(null);

		this.addWindowListener(new WindowAdapter() {

			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				//加入动作
				setVisible(false);
			}
		});
		
		//创建表模型
		dt=new DefaultTableModel(data,header){
			//设置表格内容不可被编辑
			   public boolean isCellEditable(int row, int column) {
				    return false;
				   }

		};
		jTable=new JTable(dt);//创建表格

		jTable = new JTable(dt) {

			public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
				Component component = super.prepareRenderer(renderer, row, column);

				if (row % 2 == 0) {  //将row改为column，则分列以不同颜色显示
					component.setBackground(new Color(224, 229, 223));
				}
				if (row % 2 == 1) {
					component.setBackground(new Color(184,196,177));
				}

				return component;
			}
		};

		jTable.setDefaultRenderer(Object.class, new format());
		//jTable.setEnabled(false);//内容不可更改
		jTable.setShowHorizontalLines(false);
		jTable.setShowVerticalLines(false);
		jTable.getTableHeader().setFont(new Font(null, Font.BOLD, 14));  // 设置表头名称字体样式
		jTable.getTableHeader().setForeground(Color.black);                // 设置表头名称字体颜色
//		jTable.getTableHeader().setResizingAllowed(false);               // 设置不允许手动改变列宽
		jTable.getTableHeader().setReorderingAllowed(false);//设置表头不允许拖动
		JTableHeader head =jTable.getTableHeader();
		head.setBackground(new Color(122, 133, 116));
		jTable.setRowHeight(50);

		//表头设置
		JTableHeader head1 = jTable.getTableHeader();
		head.setReorderingAllowed(false);//表头不可以动
		head.setPreferredSize(new Dimension(jTable.getColumnModel().getTotalColumnWidth(),40));

		int v=ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;//水平滚动条
		int h=ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;//垂直滚动条
		JScrollPane jsp1=new JScrollPane(jTable,v,h);//创建滚动容器
		jsp1.setBounds(20, 40, 1500, 650);
		getContentPane().add(jsp1);

	JButton btnNewButton_Alterinfo = new JButton("添加");
		btnNewButton_Alterinfo.setBounds(240, 10, 113, 27);
		btnNewButton_Alterinfo.setFocusPainted(false);//去掉按钮周围的焦点框
		btnNewButton_Alterinfo.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(btnNewButton_Alterinfo);
		btnNewButton_Alterinfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				travel_add tc=new travel_add();
				tc.setVisible(true);
				dispose();
		}
	});
//修改
    	JButton btnNewButton_Alter = new JButton("修改");
		btnNewButton_Alter.setBounds(540, 10, 113, 27);
		btnNewButton_Alter.setFocusPainted(false);//去掉按钮周围的焦点框
		btnNewButton_Alter.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(btnNewButton_Alter);
		btnNewButton_Alter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (jTable.getSelectedRow()<0) {
					JOptionPane.showMessageDialog(null, "您未选中要修改的数据！");
				} else {
					//获取用户选择的数据
					String gnum,fee,detail,gname,cneme,address,phone,origin,dest,dnum,attr;
					String rnum=jTable.getValueAt(jTable.getSelectedRow(), 0).toString();
					gnum=jTable.getValueAt(jTable.getSelectedRow(), 1).toString();
					fee=jTable.getValueAt(jTable.getSelectedRow(), 2).toString();
					detail=jTable.getValueAt(jTable.getSelectedRow(), 3).toString();
					gname=jTable.getValueAt(jTable.getSelectedRow(), 4).toString();
					cneme=jTable.getValueAt(jTable.getSelectedRow(), 5).toString();
					address=jTable.getValueAt(jTable.getSelectedRow(), 6).toString();
					phone=jTable.getValueAt(jTable.getSelectedRow(), 7).toString();
					origin=jTable.getValueAt(jTable.getSelectedRow(), 8).toString();
					dest=jTable.getValueAt(jTable.getSelectedRow(), 9).toString();
					dnum=jTable.getValueAt(jTable.getSelectedRow(), 10).toString();
					attr=jTable.getValueAt(jTable.getSelectedRow(), 11).toString();
					Tourism_information ti=new Tourism_information(gnum,fee,detail,rnum);
					Tourism_Line tl=new Tourism_Line(rnum,origin,dest,dnum,attr,gnum);
					Tourism_Group tg=new Tourism_Group(gnum,gname,cneme,address,phone);
					//打开修改界面
					travel_change tc=new travel_change(ti,tg,tl,jTable);
					tc.setVisible(true);
					dispose();
				}
			}
		});

		JButton btnNewButton_Delete = new JButton("删除");
		btnNewButton_Delete.setBounds(840, 10, 113, 27);
		btnNewButton_Delete.setFocusPainted(false);//去掉按钮周围的焦点框
		btnNewButton_Delete.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(btnNewButton_Delete);
		btnNewButton_Delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (jTable.getSelectedRow()<0) {
					JOptionPane.showMessageDialog(null, "您未选中要删除的数据！");
				} else {
					//获取用户选择的数据
					String rnum=jTable.getValueAt(jTable.getSelectedRow(), 0).toString();
					int result = JOptionPane.showConfirmDialog(null,"您确定要删除路线  “"+rnum+"”  的信息吗?","删除提示",0,1);
					if(result == JOptionPane.OK_OPTION){
						int i = updata.addData("delete tl from tourism_line tl where route_num='"+rnum+"'");
						if (i>0){
							JOptionPane.showMessageDialog(null, "路线  “"+rnum+"”  ，已被删除成功！");
						} else {
							JOptionPane.showMessageDialog(null, "删除失败！");
						}
						data=select.getTouristinfo("select * from travel_information as t left join tourism_group as tg using (group_num) left join tourism_line as tl using (route_num)");
						dt.setDataVector(data,header);
					}
				}
			}
		});
		
		JButton btnNewButton_Tip = new JButton("帮助");
		btnNewButton_Tip.setBounds(1140, 10, 113, 27);
		btnNewButton_Tip.setFocusPainted(false);//去掉按钮周围的焦点框
		btnNewButton_Tip.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(btnNewButton_Tip);
		btnNewButton_Tip.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "<html><br>修改：点击路线将会将游路线号绑定到文本框中，可以对路线信息修改。<br>删除：点击要删除的信息，点击删除即可。");
			}
		});

		/*//设置单元格内容居中显示
		DefaultTableCellRenderer r = new DefaultTableCellRenderer();   
		r.setHorizontalAlignment(JLabel.CENTER); 
		jTable.setDefaultRenderer(Object.class, r);*/

		/*TableColumnModel tcm=jTable.getColumnModel();
		TableColumn tc=tcm.getColumn(1);
		tc.setWidth(0);
		tc.setPreferredWidth(0);
		tc.setMaxWidth(0);
		tc.setMinWidth(0);
		jTable.getTableHeader().getColumnModel().getColumn(1)
				.setMaxWidth(0);
		jTable.getTableHeader().getColumnModel().getColumn(1)
				.setMinWidth(0);
		*/
	}
	
}

