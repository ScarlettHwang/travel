package View;

import javax.swing.*;

import Bean.Tourism_Group;
import Bean.Tourism_Line;
import Bean.Tourism_information;
import Bean.Tourist;
import Controller.Select;
import Controller.Updata;
import Picture.Background;
import Utils.ValidateUtils;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.event.ActionEvent;
//改变路线信息
public class travel_change extends JFrame {
    private JTextField Field_rnum;//
    private JTextField Field_gnum;//
    private JTextField Field_fee;//
    private JTextArea Field_detail;//
    private JTextField Field_gname;//
    private JTextField Field_cname;//
    private JTextField Field_address;//
    private JTextField Field_phone;//
    private JTextField Field_orgin;//
    private JTextField Field_dest;//
    private JTextField Field_dnum;//

    Select select = new Select();
    Updata updata = new Updata();
    String gnum,fee,detail,gname,cname,address,phone,origin,dest,dnum,attr;

    public travel_change(Tourism_information ti,Tourism_Group tg, Tourism_Line tl,JTable jTable) {
        super("修改路线信息");
        this.setBounds(0, 0, 650, 700);
        this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
        this.setResizable(false);//让窗口大小不可改变
        this.getContentPane().setBackground(new Color(176, 211, 238, 207));
        getContentPane().setLayout(null);
//关闭修改窗口，跳到全部详情界面
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                //加入动作
                travel_Management b = new travel_Management();
                b.setVisible(true);
            }
        });

        JLabel lblNewLabel_rnum= new JLabel("路线号：");
        lblNewLabel_rnum.setBounds(138, 79, 72, 18);
        getContentPane().add(lblNewLabel_rnum);
        Field_rnum = new JTextField();
        Field_rnum.setBounds(191, 76, 240, 24);
        getContentPane().add(Field_rnum);
        Field_rnum.setColumns(10);
        //Field_rnum.setEnabled(false);

        JLabel lblNewLabel_fee= new JLabel("费用：");
        lblNewLabel_fee.setBounds(138, 125, 72, 18);
        getContentPane().add(lblNewLabel_fee);
        Field_fee = new JTextField();
        Field_fee.setBounds(191, 123, 240, 24);
        getContentPane().add(Field_fee);
        Field_fee.setColumns(10);

        JLabel lblNewLabel_dnum= new JLabel("天数：");
        lblNewLabel_dnum.setBounds(138, 168, 72, 18);
        getContentPane().add(lblNewLabel_dnum);
        Field_dnum = new JTextField();
        Field_dnum.setBounds(191, 165, 240, 24);
        getContentPane().add(Field_dnum);
        Field_dnum.setColumns(10);

/*        JLabel lblNewLabel_cname= new JLabel("导游名：");
        lblNewLabel_cname.setBounds(138, 213, 72, 18);
        getContentPane().add(lblNewLabel_cname);
        Field_cname = new JTextField();
        Field_cname.setBounds(191, 210, 240, 24);
        getContentPane().add(Field_cname);
        Field_cname.setColumns(10);

        JLabel lblNewLabel_address= new JLabel("地址：");
        lblNewLabel_address.setBounds(138, 254, 72, 18);
        getContentPane().add(lblNewLabel_address);
        Field_address = new JTextField();
        Field_address.setBounds(191, 251, 240, 24);
        getContentPane().add(Field_address);
        Field_address.setColumns(10);

        JLabel lblNewLabel_phone= new JLabel("电话：");
        lblNewLabel_phone.setBounds(138, 295, 72, 18);
        getContentPane().add(lblNewLabel_phone);
        Field_phone = new JTextField();
        Field_phone.setBounds(191, 292, 240, 24);
        getContentPane().add(Field_phone);
        Field_phone.setColumns(10);*/

        JLabel lblNewLabel_orgin= new JLabel("出发地：");
        lblNewLabel_orgin.setBounds(138, 213, 72, 18);
        getContentPane().add(lblNewLabel_orgin);
        Field_orgin = new JTextField();
        Field_orgin.setBounds(191, 210, 240, 24);
        getContentPane().add(Field_orgin);
        Field_orgin.setColumns(10);

        JLabel lblNewLabel_dest= new JLabel("目的地：");
        lblNewLabel_dest.setBounds(138, 254, 72, 18);
        getContentPane().add(lblNewLabel_dest);
        Field_dest = new JTextField();
        Field_dest.setBounds(191, 251, 240, 24);
        getContentPane().add(Field_dest);
        Field_dest.setColumns(10);

        JLabel lblNewLabel_attr= new JLabel("主要景点：");
        lblNewLabel_attr.setBounds(128, 333, 72, 18);
        getContentPane().add(lblNewLabel_attr);
        JTextField  Field_attr = new JTextField();
        Field_attr.setBounds(191, 329, 240, 24);
        getContentPane().add(Field_attr);
        Field_attr.setColumns(10);
        //Field_attr.setEnabled(false);

        JLabel lblNewLabel_detail= new JLabel("详细信息：");
        lblNewLabel_detail.setBounds(128, 378, 117, 18);
        getContentPane().add(lblNewLabel_detail);
        Field_detail = new JTextArea();
        Field_detail.setLineWrap(true);    //设置文本域中的文本为自动换行
        Field_detail.setFont(new Font("楷体",Font.PLAIN,14));    //修改字体样式
        JScrollPane jsp=new JScrollPane(Field_detail);    //将文本域放入滚动窗口
        jsp.setBounds(191,376,300,150);
        getContentPane().add(jsp);    //将JPanel容器添加到JFrame容器中
        this.setVisible(true);

//下拉框选团号
        JLabel lblNewLabel_gname= new JLabel("团号：");
        lblNewLabel_gname.setBounds(138, 295, 72, 18);
        getContentPane().add(lblNewLabel_gname);
        JComboBox comboBox_gname= new JComboBox();
       /* Object[] data=select.getGnum("select group_num from tourism_group");
        comboBox_gname.setModel(new DefaultComboBoxModel(data));*/
        comboBox_gname.setModel(new DefaultComboBoxModel(new String[] {"233", "666"}));
        comboBox_gname.setBounds(191, 292, 240, 24);
        getContentPane().add(comboBox_gname);

        ImageIcon i0 = new ImageIcon("img/Icon0.png");
        JButton lvt = new JButton("查看旅游团信息",i0);
        lvt.setBounds(455, 290, 158, 27);
        lvt.setFocusPainted(false);//去掉按钮周围的焦点框
        lvt.setContentAreaFilled(false);//设置按钮bu透明背景
        getContentPane().add(lvt);
        lvt.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                Enquiry e = new Enquiry();
                e.setVisible(true);
                //dispose();
            }
        });

//将获得的数据绑定到对应文本框内
        Field_rnum.setText(ti.getRoute_num());
        //Field_gnum.setText(ti.getGroup_num());
        Field_fee.setText(ti.getFee());
        Field_detail.setText(ti.getDetail());
       /* Field_cname.setText(tg.getGroup_contact());
        Field_address.setText(tg.getGroup_address());//
        Field_phone.setText(tg.getGroup_phone());//*/
        Field_orgin.setText(tl.getOrigin());//
        Field_dest.setText(tl.getDestination());//
        Field_dnum.setText(tl.getDay_num());//
        String tgnum = tg.getGroup_num();
        comboBox_gname.setSelectedItem(tgnum);
        Field_attr.setText(tl.getAttractions());

        JButton btnNewButton = new JButton("确认修改");
        btnNewButton.setBounds(230, 560, 124, 33);
        btnNewButton.setFocusPainted(false);//去掉按钮周围的焦点框
        btnNewButton.setContentAreaFilled(false);//设置按钮透明背景
        getContentPane().add(btnNewButton);
        btnNewButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //获取文本框内的值
                String nrnum = Field_rnum.getText();
                String rnum=jTable.getValueAt(jTable.getSelectedRow(), 0).toString();
                //gnum = Field_gnum.getText();
                fee = Field_fee.getText();
                detail = Field_detail.getText();
                gname = comboBox_gname.getSelectedItem().toString();
                origin = Field_orgin.getText();
                dest = Field_dest.getText();
                dnum = Field_dnum.getText();
                attr =Field_attr.getText();


                //判断输入的信息是否为空，是否完整
                if (fee.equals("") || dnum.equals("")  || origin.equals("") || dest.equals("") || detail.equals("")) {
                    JOptionPane.showMessageDialog(null, "请输入完整信息！");
                } else {
                    String sql2="select COUNT(*) from tourism_line where route_num='"+nrnum+"' ";
                    int exitNum = select.getCount(sql2);
                    //有匹配的重复信息，且新从框内获取的数据与框内原来的数据不一样
                    //一样就说明没改路线号，这是允许的
                    if(exitNum>0&&!nrnum.equals(rnum)){
                        JOptionPane.showMessageDialog(null, "路线号重复！请重新输入！");
                    }else {
                    Pattern p = Pattern.compile("[1-9]*");
                    Matcher m = p.matcher(dnum);
                    if (!m.matches()){
                        JOptionPane.showMessageDialog(null, "天数输入有误");
                    }else {
                        Pattern p2 = Pattern.compile("^(0|(0.[1-9])|(0.[0-9]{1,4}[^0])|([1-9][0-9]*)+(.[0-9]{1,5})|([1-9][0-9]*))?$");
                        Matcher m2 = p2.matcher(fee);
                        if (!m2.matches()) {
                            JOptionPane.showMessageDialog(null, "费用输入有误！请输入正数！");
                        } else {

                            String sql = "UPDATE tourism_line SET route_num='" + nrnum + "',origin='" + origin + "',destination='" + dest + "',day_num='" + dnum + "',group_num='" + gname + "',attractions='" + attr + "'  WHERE route_num='" + rnum + "';";
                            int result = updata.addData(sql);
                            String sql3 = "UPDATE travel_information SET route_num='" + nrnum + "',fee='" + fee + "',group_num='" + gname + "',detail='" + detail + "' WHERE route_num='" + rnum + "';";
                            int result2 = updata.addData(sql3);

                            //判断插入结果
                            if (result > 0 || result2 > 0) {
                                JOptionPane.showMessageDialog(null, "修改成功！");
                                travel_Management tm = new travel_Management();
                                //tm.dispose();
                                tm.setVisible(true);
                                dispose();

                            } else {
                                JOptionPane.showMessageDialog(null, "修改失败，请与管理员联系！");
                            }

                        }
                    }
                    }
                }
            }
        });

    }

}
