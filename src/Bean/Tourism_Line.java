package Bean;
 
//旅游线路表
public class Tourism_Line {
	String route_num;//路线号
	String origin;//起点
	String destination;//终点
	String day_num;//天数
	String attractions;//主要景点
	String group_num;//团号
	@Override
	public String toString() {
		return "Tourism_Line [route_num=" + route_num + ", origin=" + origin + ", destination=" + destination
				+ ", day_num=" + day_num + ", attractions=" + attractions + ",group_num="+group_num+"]";
	}
	public Tourism_Line(String route_num, String origin, String destination, String day_num, String attractions,String group_num) {
		super();
		this.route_num = route_num;
		this.origin = origin;
		this.destination = destination;
		this.day_num = day_num;
		this.attractions = attractions;
		this.group_num=group_num;
	}
	public Tourism_Line() {
		super();
	}
	public String getRoute_num() {
		return route_num;
	}
	public void setRoute_num(String route_num) {
		this.route_num = route_num;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getDay_num() {
		return day_num;
	}
	public void setDay_num(String day_num) {
		this.day_num = day_num;
	}
	public String getAttractions() {
		return attractions;
	}
	public void setAttractions(String attractions) {
		this.attractions = attractions;
	}
	public String getGroup_num() {
		return group_num;
	}
	public void setGroup_num(String group_num) {
		this.group_num = group_num;
	}
	
}