package View;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import Controller.Select;
import Controller.Updata;
import Picture.Background;

public class Enquiry extends JFrame {
		
		Select select = new Select();
		Updata updata = new Updata();
		JButton btnNewButton_Export;
		JTable jTable;
		DefaultTableModel dt;

		Object[] header= {"团号","团名","导游","地址","电话"};
		Object[][] data=select.getTourism_Group("SELECT * FROM tourism_group");
		
		public Enquiry() {
			super("旅游团信息");
			this.setBounds(0, 0, 1200, 300);
			this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
			this.setResizable(false);//让窗口大小不可改变
			getContentPane().setLayout(null);
			
			
			//创建表模型
			DefaultTableModel dt=new DefaultTableModel(data,header){
				//设置表格内容不可被编辑
				   public boolean isCellEditable(int row, int column) {
					    return false;//返回true表示能编辑，false表示不能编辑
					   }
			};
			JTable jTable=new JTable(dt);//创建表格
			jTable.getTableHeader().setFont(new Font(null, Font.BOLD, 14));  // 设置表头名称字体样式
			jTable.getTableHeader().setForeground(Color.red);                // 设置表头名称字体颜色
			jTable.getTableHeader().setResizingAllowed(false);               // 设置不允许手动改变列宽
			jTable.getTableHeader().setReorderingAllowed(false);//设置表头不允许拖动
			int v=ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;//水平滚动条
			int h=ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;//垂直滚动条
			JScrollPane jsp=new JScrollPane(jTable,v,h);//创建滚动容器
			jsp.setBounds(14, 68, 1166, 200);
			getContentPane().add(jsp);
			//设置单元格内容居中显示
			DefaultTableCellRenderer r = new DefaultTableCellRenderer();   
			r.setHorizontalAlignment(JLabel.CENTER); 
			jTable.setDefaultRenderer(Object.class, r);
			
		}
}