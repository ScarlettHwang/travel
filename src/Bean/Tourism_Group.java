package Bean;
 
//旅游团表
public class Tourism_Group {
	String group_num;//团号
	String group_name;//团名
	String group_contact;//联系人
	String group_address;//地址
	String group_phone;//电话
	@Override
	public String toString() {
		return "Tourism_Group [group_num=" + group_num + ", group_name=" + group_name + ", group_contact="
				+ group_contact + ", group_address=" + group_address + ", group_phone=" + group_phone + "]";
	}
	public Tourism_Group(String group_num, String group_name, String group_contact, String group_address,
			String group_phone) {
		super();
		this.group_num = group_num;
		this.group_name = group_name;
		this.group_contact = group_contact;
		this.group_address = group_address;
		this.group_phone = group_phone;
	}
	public Tourism_Group() {
		super();
	}
	public String getGroup_num() {
		return group_num;
	}
	public void setGroup_num(String group_num) {
		this.group_num = group_num;
	}
	public String getGroup_name() {
		return group_name;
	}
	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}
	public String getGroup_contact() {
		return group_contact;
	}
	public void setGroup_contact(String group_contact) {
		this.group_contact = group_contact;
	}
	public String getGroup_address() {
		return group_address;
	}
	public void setGroup_address(String group_address) {
		this.group_address = group_address;
	}
	public String getGroup_phone() {
		return group_phone;
	}
	public void setGroup_phone(String group_phone) {
		this.group_phone = group_phone;
	}
	
}