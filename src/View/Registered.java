package View;
 
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
 
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
 
import Controller.Updata;
import Picture.Background;
 
public class Registered extends JFrame {
	Updata updata = new Updata();
	private JTextField textField_zh;
	private JPasswordField textField_srmm;
	private JPasswordField textField_qrmm;
	public Registered() {
		super.setTitle("账号注册");
		this.setBounds(0, 0, 700, 550);//设置大小
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
  	    this.setResizable(false);//让窗口大小不可改变
		getContentPane().setLayout(null);
		
	/*	this.addWindowListener(new WindowAdapter() {
			 
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				//加入动作
				Background b = new Background();
				b.setVisible(true);
			 }
		});*/
		
		JLabel label_zh = new JLabel("账号名：");
		label_zh.setFont(new Font("宋体", Font.CENTER_BASELINE, 15));
		label_zh.setBounds(165, 138, 72, 18);
		getContentPane().add(label_zh);
		
		textField_zh = new JTextField();
		textField_zh.setBounds(248, 130, 255, 34);
		getContentPane().add(textField_zh);
		textField_zh.setColumns(10);
		
		JLabel label_srmm = new JLabel("输入密码：");
		label_srmm.setFont(new Font("宋体", Font.CENTER_BASELINE, 15));
		label_srmm.setBounds(165, 208, 83, 18);
		getContentPane().add(label_srmm);
		
		textField_srmm = new JPasswordField();
		textField_srmm.setBounds(248, 267, 255, 34);
		getContentPane().add(textField_srmm);
		textField_srmm.setColumns(10);
		
		JLabel label_qrmm = new JLabel("确认密码：");
		label_qrmm.setFont(new Font("宋体", Font.CENTER_BASELINE, 15));
		label_qrmm.setBounds(165, 275, 92, 18);
		getContentPane().add(label_qrmm);
		
		textField_qrmm = new JPasswordField();
		textField_qrmm.setBounds(248, 200, 255, 34);
		getContentPane().add(textField_qrmm);
		textField_qrmm.setColumns(10);
		
		JButton button = new JButton("注册");
		button.setFont(new Font("宋体", Font.BOLD, 20));
		button.setBounds(282, 362, 113, 34);
		button.setFocusPainted(false);//去掉按钮周围的焦点框
		button.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(button);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String yhm = textField_zh.getText();
				String srmm = textField_srmm.getText();
				String qrmm = textField_qrmm.getText();
				//确认输入的信息是否为空
				if (yhm.equals("")&&srmm.equals("")&&qrmm.equals("")) {
					JOptionPane.showMessageDialog(null, "请完整输入信息！");
				}else {
					//判断两次密码是否一致
					if (srmm.equals(qrmm)) {
						String sql = "INSERT INTO `user` VALUES (null, '"+yhm+"', '"+srmm+"', '游客', '未登录');";
						int reselt = updata.addData(sql);
						if (reselt>0) {
							JOptionPane.showMessageDialog(null, "注册成功！将跳转到登录页面！");
							Login l = new Login();
							l.setVisible(true);
							dispose();
						}else {
							JOptionPane.showMessageDialog(null, "注册失败！");
						}
					}else {
						JOptionPane.showMessageDialog(null, "两次输入的密码不一致！请检查！");
					}
				}
			}
		});
	}
 
}