package Controller;
 
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Bean.Tourism_Group;
import Bean.Tourism_Line;
import Bean.Tourism_information;
import Bean.Tourist;
import Dao.DbConnection;
 
public class Select{
	//查询数据条数
	public int getCount(String sql) {
		ResultSet resultSet=DbConnection.query(sql);
		try {
			if (resultSet.next()) {
				return resultSet.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	//得到一条数据
	public String getString(String sql) {
		ResultSet resultSet=DbConnection.query(sql);
		try {
			if (resultSet.next()) {
				return resultSet.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	//用户登录
	public int Select(String account ,String password) {
		String sql="select * from UserInfo where password='"+password+"'  and account='"+account+"'";
		ResultSet resultSet=DbConnection.query(sql);
		int a=0;
		try {
			while (resultSet.next()) {
				a=1;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return a;
	}
	//旅游线路信息表
	public Object[][] getLineInfo() {
		String sql="SELECT * FROM tourism_line";
		ResultSet resultSet = DbConnection.query(sql);
		ArrayList<Tourism_Line> list=new ArrayList<Tourism_Line>();
		try {
			while (resultSet.next()) {
				Tourism_Line t=new Tourism_Line();
				t.setRoute_num(resultSet.getString(1));
				t.setOrigin(resultSet.getString(2));
				t.setDestination(resultSet.getString(3));
				t.setDay_num(resultSet.getString(4));
				t.setAttractions(resultSet.getString(5));
				t.setGroup_num(resultSet.getString(6));
				list.add(t);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Object[][] objects=new Object[list.size()][6];
		for(int i=0;i<list.size();i++) {
			objects[i][0]=list.get(i).getRoute_num();
			objects[i][1]=list.get(i).getOrigin();
			objects[i][2]=list.get(i).getDestination();
			objects[i][3]=list.get(i).getDay_num();
			objects[i][4]=list.get(i).getAttractions();
			objects[i][5]=list.get(i).getGroup_num();
		}
		return objects;
	}
	//获取团号
	public Object[] getGnum(String sql) {
		ResultSet resultSet = DbConnection.query(sql);
		ArrayList<Tourism_Group> list=new ArrayList<Tourism_Group>();
		try {
			while (resultSet.next()) {
				Tourism_Group tg=new Tourism_Group();
				tg.setGroup_num(resultSet.getString(1));
				list.add(tg);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Object[] objects=new Object[list.size()];
		for(int i=0;i<list.size();i++) {
			objects[i]=list.get(i).getGroup_num();
		}
		return objects;
	}
	//旅游报名信息表
	public Object[][] getTourist(String sql) {
//		String sql="SELECT * FROM tourist";
		ResultSet resultSet = DbConnection.query(sql);
		ArrayList<Tourist> list=new ArrayList<Tourist>();
		try {
			while (resultSet.next()) {
				Tourist t=new Tourist();
				t.setTourist_num(resultSet.getString(1));
				t.setUser_id(resultSet.getString(2));
				t.setTourist_name(resultSet.getString(3));
				t.setTourist_sex(resultSet.getString(4));
				t.setTourist_age(resultSet.getString(5));
				t.setTourist_idCard(resultSet.getString(6));
				t.setTourist_address(resultSet.getString(7));
				t.setTourist_phone(resultSet.getString(8));
				t.setGroup_name(resultSet.getString(9));
				t.setAccompanied(resultSet.getString(10));
				t.setAccommodation(resultSet.getString(11));
				t.setRoute_num(resultSet.getString(12));
				list.add(t);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Object[][] objects=new Object[list.size()][11];
		for(int i=0;i<list.size();i++) {
			objects[i][0]=list.get(i).getTourist_num();
    //			objects[i][1]=list.get(i).getUser_id();
			objects[i][1]=list.get(i).getTourist_name();
			objects[i][2]=list.get(i).getTourist_sex();
			objects[i][3]=list.get(i).getTourist_age();
			objects[i][4]=list.get(i).getTourist_idCard();
			objects[i][5]=list.get(i).getTourist_address();
			objects[i][6]=list.get(i).getTourist_phone();
			objects[i][7]=list.get(i).getGroup_name();
			objects[i][8]=list.get(i).getAccompanied();
			objects[i][9]=list.get(i).getAccommodation();
			objects[i][10]=list.get(i).getRoute_num();
		}
		return objects;
	}
	//旅游信息表
		public Object[][] getTouristinfo(String sql) {
//
			ResultSet resultSet = DbConnection.query(sql);
			ArrayList<Tourism_Line> list=new ArrayList<Tourism_Line>();
			ArrayList<Tourism_information> list1=new ArrayList<Tourism_information>();
			ArrayList<Tourism_Group> list2=new ArrayList<Tourism_Group>();
			try {
				//从结果集里获取信息赋值给变量
				while (resultSet.next()) {
					Tourism_Line t=new Tourism_Line();
					Tourism_Group tg=new Tourism_Group();
					Tourism_information ti=new Tourism_information();
					ti.setRoute_num(resultSet.getString(1));
					ti.setGroup_num(resultSet.getString(2));
					ti.setFee(resultSet.getString(3));
					ti.setDetail(resultSet.getString(4));
					tg.setGroup_name(resultSet.getString(5));
					tg.setGroup_contact(resultSet.getString(6));
					tg.setGroup_address(resultSet.getString(7));
					tg.setGroup_phone(resultSet.getString(8));
					t.setOrigin(resultSet.getString(9));
					t.setDestination(resultSet.getString(10));
					t.setDay_num(resultSet.getString(11));
					t.setAttractions(resultSet.getString(12));
					//放集合框架里
					list.add(t);
					list1.add(ti);
					list2.add(tg);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			//从集合框架取出来放二维数组里
			Object[][] objects=new Object[list.size()][12];
			for(int i=0;i<list1.size();i++) {
				objects[i][0]=list1.get(i).getRoute_num();
				objects[i][1]=list1.get(i).getGroup_num();
				objects[i][2]=list1.get(i).getFee();
				objects[i][3]=list1.get(i).getDetail();
			}
			for(int i=0;i<list2.size();i++) {
				objects[i][4]=list2.get(i).getGroup_name();
				objects[i][5]=list2.get(i).getGroup_contact();
				objects[i][6]=list2.get(i).getGroup_address();
				objects[i][7]=list2.get(i).getGroup_phone();
			}
			for(int i=0;i<list.size();i++) {

				objects[i][8]=list.get(i).getOrigin();
				objects[i][9]=list.get(i).getDestination();
				objects[i][10]=list.get(i).getDay_num();
				objects[i][11]=list.get(i).getAttractions();
			}
			return objects;
		}

	//旅游团信息表
		public Object[][] getTourism_Group(String sql) {
//			String sql="SELECT * FROM tourism_group";
			ResultSet resultSet = DbConnection.query(sql);
			ArrayList<Tourism_Group> list=new ArrayList<Tourism_Group>();
			try {
				while (resultSet.next()) {
					Tourism_Group t=new Tourism_Group();
					t.setGroup_num(resultSet.getString(1));
					t.setGroup_name(resultSet.getString(2));
					t.setGroup_contact(resultSet.getString(3));
					t.setGroup_address(resultSet.getString(4));
					t.setGroup_phone(resultSet.getString(5));
					
					list.add(t);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			Object[][] objects=new Object[list.size()][5];
			for(int i=0;i<list.size();i++) {
				objects[i][0]=list.get(i).getGroup_num();
				objects[i][1]=list.get(i).getGroup_name();
				objects[i][2]=list.get(i).getGroup_contact();
				objects[i][3]=list.get(i).getGroup_address();
				objects[i][4]=list.get(i).getGroup_phone();
				
			}
			return objects;
		}
	
}