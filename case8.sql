/*
Navicat MySQL Data Transfer

Source Server         : logTest
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : case8

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2021-12-27 12:20:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tourism_group
-- ----------------------------
DROP TABLE IF EXISTS `tourism_group`;
CREATE TABLE `tourism_group` (
  `group_num` int(10) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(20) NOT NULL,
  `group_contact` varchar(20) NOT NULL,
  `group_address` varchar(20) NOT NULL,
  `group_phone` varchar(20) NOT NULL,
  PRIMARY KEY (`group_num`)
) ENGINE=InnoDB AUTO_INCREMENT=667 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tourism_group
-- ----------------------------
INSERT INTO `tourism_group` VALUES ('233', '随心所欲旅行团', '小李', '天津', '13125587654');
INSERT INTO `tourism_group` VALUES ('666', '夕阳红旅行团', '嵩秉义', '中国香港', '13125567899');

-- ----------------------------
-- Table structure for tourism_line
-- ----------------------------
DROP TABLE IF EXISTS `tourism_line`;
CREATE TABLE `tourism_line` (
  `route_num` int(10) NOT NULL AUTO_INCREMENT COMMENT '线路号',
  `origin` varchar(30) NOT NULL,
  `destination` varchar(30) NOT NULL,
  `day_num` int(10) NOT NULL,
  `attractions` varchar(30) NOT NULL,
  `group_num` int(10) NOT NULL,
  PRIMARY KEY (`route_num`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of tourism_line
-- ----------------------------
INSERT INTO `tourism_line` VALUES ('1', '西安', '丽江', '3', '丽江古城', '233');
INSERT INTO `tourism_line` VALUES ('2', '西安', '大理', '5', '大理古城', '666');
INSERT INTO `tourism_line` VALUES ('3', '沈阳', '上海', '6', '世博园', '233');
INSERT INTO `tourism_line` VALUES ('4', '沈阳', '南京', '7', '南京博物院', '666');
INSERT INTO `tourism_line` VALUES ('5', '沈阳', '甘肃', '4', '莫高窟', '233');
INSERT INTO `tourism_line` VALUES ('6', '沈阳', '深圳', '9', '世界之窗', '666');
INSERT INTO `tourism_line` VALUES ('7', '沈阳', '丹东', '3', '鸭绿江断桥', '233');
INSERT INTO `tourism_line` VALUES ('8', '沈阳', '杭州', '7', '西湖', '666');
INSERT INTO `tourism_line` VALUES ('9', '沈阳', '本溪', '4', '水洞', '233');
INSERT INTO `tourism_line` VALUES ('10', '西安', '甘肃', '10', '莫高窟', '233');
INSERT INTO `tourism_line` VALUES ('11', '西安', '湖南', '5', '橘子洲', '666');
INSERT INTO `tourism_line` VALUES ('12', '西安', '上海', '3', '世博园', '233');
INSERT INTO `tourism_line` VALUES ('13', '西安', '南京', '6', '南京博物院', '666');
INSERT INTO `tourism_line` VALUES ('14', '西安', '上海', '7', '黄浦江', '233');

-- ----------------------------
-- Table structure for tourist
-- ----------------------------
DROP TABLE IF EXISTS `tourist`;
CREATE TABLE `tourist` (
  `tourist_num` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `tourist_name` varchar(20) NOT NULL,
  `tourist_sex` char(10) NOT NULL,
  `tourist_age` int(20) NOT NULL,
  `tourist_idcard` varchar(20) NOT NULL,
  `tourist_address` varchar(50) NOT NULL,
  `tourist_phone` varchar(20) NOT NULL,
  `group_name` varchar(20) NOT NULL,
  `accompanied` varchar(20) NOT NULL,
  `accommodation` varchar(20) NOT NULL,
  `route_num` int(10) NOT NULL,
  PRIMARY KEY (`tourist_num`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tourist
-- ----------------------------
INSERT INTO `tourist` VALUES ('1', '1', '李华', '男', '21', '123456789123456789', '云南', '15596961236', '随心所欲旅行团', '是', '是', '1');
INSERT INTO `tourist` VALUES ('2', '2', '王明', '男', '21', '560250255504153389', '北京市三里屯望风小区', '18893932636', '夕阳红旅行团', '是', '是', '2');
INSERT INTO `tourist` VALUES ('3', '3', '杨明辉', '女', '21', '560250255504153389', '北京市三里屯', '18896365621', '随心所欲旅行团', '是', '是', '10');
INSERT INTO `tourist` VALUES ('4', '1', '刘毅都', '男', '22', '526369522201451125', '上海市黄浦江', '18893932936', '夕阳红旅行团', '是', '无', '11');
INSERT INTO `tourist` VALUES ('5', '1', '沈玉', '男', '25', '610203200004160029', '绿藤市', '18896965632', '夕阳红旅行团', '无', '无', '13');
INSERT INTO `tourist` VALUES ('6', '5', '马漂浮', '男', '60', '632036200321455892', '杭州市', '18896965632', '夕阳红旅行团', '无', '是', '13');
INSERT INTO `tourist` VALUES ('7', '1', '灵玉', '女', '21', '610203200004152206', '上海市', '15563632369', '夕阳红旅行团', '是', '无', '13');
INSERT INTO `tourist` VALUES ('8', '6', '礼遇', '男', '63', '620304522202362201', '云南', '15563631258', '夕阳红旅行团', '是', '无', '13');
INSERT INTO `tourist` VALUES ('9', '3', '芳华', '女', '22', '610230522202360029', '天津', '15536362598', '随心所欲旅行团', '是', '是', '16');
INSERT INTO `tourist` VALUES ('14', '1', '荷兰', '男', '32', '630254200004156632', '香港', '18562362549', '夕阳红旅行团', '是', '是', '17');
INSERT INTO `tourist` VALUES ('15', '1', '克鲁格', '男', '42', '630204200001453365', '北京', '18869654236', '夕阳红旅行团', '是', '是', '17');
INSERT INTO `tourist` VALUES ('16', '1', '珂兰', '女', '25', '610236266631453369', '陕西省西安市', '15536364598', '夕阳红旅行团', '是', '无', '17');
INSERT INTO `tourist` VALUES ('17', '1', '毛利兰', '女', '18', '630124200024254415', '米花市', '18845254897', '夕阳红旅行团', '是', '是', '17');
INSERT INTO `tourist` VALUES ('23', '1', '李煜', '男', '21', '610232200004162235', '沈阳市', '15563632958', '随心所欲旅行团', '是', '是', '20');
INSERT INTO `tourist` VALUES ('24', '1', '陈树楷', '男', '21', '265412322221485526', '上海市', '18896962698', '夕阳红旅行团', '是', '无', '21');
INSERT INTO `tourist` VALUES ('26', '7', '杨坤', '男', '32', '523654899995451123', '云南', '18839632654', '随心所欲旅行团', '是', '是', '20');
INSERT INTO `tourist` VALUES ('27', '1', '李泽楷', '男', '20', '610203222220123224', '山西省', '18896965412', '随心所欲旅行团', '是', '是', '1');

-- ----------------------------
-- Table structure for travel_information
-- ----------------------------
DROP TABLE IF EXISTS `travel_information`;
CREATE TABLE `travel_information` (
  `route_num` int(10) NOT NULL,
  `fee` varchar(10) DEFAULT NULL,
  `group_num` int(10) DEFAULT NULL,
  `detail` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`route_num`),
  KEY `group_num` (`group_num`),
  CONSTRAINT `travel_information_ibfk_1` FOREIGN KEY (`route_num`) REFERENCES `tourism_line` (`route_num`),
  CONSTRAINT `travel_information_ibfk_2` FOREIGN KEY (`group_num`) REFERENCES `tourism_group` (`group_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of travel_information
-- ----------------------------
INSERT INTO `travel_information` VALUES ('1', '1000.11', '233', 'D1\n丽江古城 > 狮子山 > 木府 > 丽江四方街\r\n\r\nD2\r\n冰川公园 > 云杉坪 > 蓝月谷\r\n\r\nD3\r\n束河古镇 > 束河四方街 > 束河古镇-青龙桥 > 九鼎龙潭 > 三圣宫 > 茶马古道博物馆\r\n\r\nD4\r\n拉市海湿地公园');
INSERT INTO `travel_information` VALUES ('2', '10000', '666', 'DAY1·大理\r\n\n在大理古城坠入慵懒时光详情\r\n\n大理古城－崇圣寺三塔文化旅游区－三塔倒影公园－人民路\r\nDAY2·大理\r\n\n环游洱海寻秘境详情\r\n\n大理古城－喜洲古镇－蝴蝶泉－双廊古镇－\r\n南诏风情岛－挖色镇－小普陀－大理古城\r\n\nDAY3·大理\r\n\n登苍山感受云雾变幻苍山');
INSERT INTO `travel_information` VALUES ('3', '7000', '233', 'DAY1·上海\r\n感受摩登与古典的完美融合\r\n上海城隍庙旅游区－豫园－南京路步行街外滩\r\nDAY2·上海\r\n购物逛吃好去处\r\n中华艺术宫－田子坊－上海新天地\r\nＤ3·上海\r\n踏入“迪士尼”的童话世界\r\n上海迪士尼度假区\r\nDAY4·上海\r\n打卡地标东方明珠\r\n上海杜莎夫人蜡像馆－陆家嘴－东方明珠广播电视塔');
INSERT INTO `travel_information` VALUES ('4', '8000', '666', 'DAY1·南京\r\n《致青春》里的校园时光\r\n总统府－南京大学（鼓楼校区）－古鸡鸣寺－玄\r\n武湖公园－狮子桥美食步行街\r\nDAY2·南京\r\n中山陵缅怀国父\r\n中山陵－钟山风景区－明孝陵景区－夫子\r\n庙－秦淮风光带\r\nＤ3·南京\r\n缅怀同胞，致敬英雄\r\n侵华日军南京大屠杀遇难同胞纪念馆－雨花台风景区');
INSERT INTO `travel_information` VALUES ('5', '5000', '233', 'DAY1·西宁-青海湖\r\n朝圣塔尔寺，漫步青海湖\r\n塔尔寺－日月山－青海湖\r\nDAY2·青海湖-祁连\r\n打卡天空之境，遇见醉美祁连\r\n茶卡盐湖－环湖西路－刚察草原－大冬树山垭口—祁连山草原\r\nDAY3·祁连-张掖\r\n打卡祁连山，守候七彩丹霞落日\r\n阿柔大寺－卓尔山－张掖七彩丹霞旅游景区\r\nDAY4·门源-西宁\r\n花开花落马蹄寺，门源花海\r\n马蹄寺－门源百里油菜花海');
INSERT INTO `travel_information` VALUES ('6', '10000', '666', 'DAY1·深圳\r\n去华侨城拍照\r\n何香凝美术馆－华侨城生态广场－深圳世界之窗\r\nDAY2·深圳\r\n在彩色小镇打卡拍照\r\n深圳大学－深业上城－莲花山公园\r\nDAY3·深圳\r\n网红植物园\r\n杨梅坑－深圳仙湖植物园');
INSERT INTO `travel_information` VALUES ('7', '2000', '233', 'DAY\r\n丹东月亮岛绿江断桥丹东河口边境鸭绿江河口码头内河游船安东老街\r\n01');
INSERT INTO `travel_information` VALUES ('8', '6000', '666', 'Ｄ1·杭州\r\n以喝茶为名浸润西湖\r\n断桥残雪－白堤－孤山－楼外楼（孤山路店）曲院风荷－苏堤春晓－花港观鱼－茅家埠\r\nDAY2·杭州\r\n雷峰塔下寻白娘子与许仙的动人传说\r\n柳浪闻莺－雷峰塔－三潭印月－吴山广场河坊街－西湖－景区音乐喷泉\r\nDAY3·杭州\r\n去灵隐寺祈福\r\n灵隐飞来峰－灵隐寺－龙井村－九溪烟树－钱塘江大桥\r\nDAY4·杭州\r\n隐于园林中的浙大校园\r\n西溪国家湿地公园－浙江大学玉泉校区－青芝坞');
INSERT INTO `travel_information` VALUES ('9', '1000', '233', '第一天：沈阳市区出发，大约两小时到达本溪大峡谷，以高空体验项目为主，难度系数较低，侧重游玩（路线：高空飞翼、七彩滑道、天空之镜、玻璃吊桥、厄洛斯之手、天梯、悬崖秋千、幻影之城）。\r\n第二天：体验高空冒险项目，难度系数较高，侧重冒险（路线：龙吟栈道、巴图鲁蹦极、新西兰秋千、丛林滑道或者虎啸栈道、七彩云端水滑道）。结束后返程。');
INSERT INTO `travel_information` VALUES ('10', '5000', '233', 'DAY1·西宁-青海湖\r\n朝圣塔尔寺，漫步青海湖\r\n塔尔寺－日月山－青海湖\r\nDAY2·青海湖-祁连\r\n打卡天空之境，遇见醉美祁连\r\n茶卡盐湖－环湖西路－刚察草原－大冬树山垭口—祁连山草原\r\nDAY3·祁连-张掖\r\n打卡祁连山，守候七彩丹霞落日\r\n阿柔大寺－卓尔山－张掖七彩丹霞旅游景区\r\nDAY4·门源-西宁\r\n花开花落马蹄寺，门源花海\r\n马蹄寺－门源百里油菜花海');
INSERT INTO `travel_information` VALUES ('11', '1500', '666', 'DAY1·张家界\r\n登高远眺天子山\r\n张家界武陵源风景名胜区－天子山索道－天子观景台－御笔峰－神兵聚会－张家界十里画廊－溪布街\r\nDAY2·张家界\r\n畅玩醉美金鞭溪\r\n张家界国家森林公园－金鞭溪－都正街\r\nDAY3·长沙\r\n初次邂逅长沙城\r\n湖南大学－岳麓书院－湖南省立第一师范学\r\n校旧址－杜甫江阁－太平街\r\nDAY4·长沙\r\n打卡天心阁和简牍博物馆\r\n天心阁－长沙简牍博物馆');
INSERT INTO `travel_information` VALUES ('12', '3000', '233', 'DAY1·上海\r\n感受摩登与古典的完美融合\r\n上海城隍庙旅游区－豫园－南京路步行街外滩\r\nDAY2·上海\r\n购物逛吃好去处\r\n中华艺术宫－田子坊－上海新天地\r\nＤ3·上海\r\n踏入“迪士尼”的童话世界\r\n上海迪士尼度假区\r\nDAY4·上海\r\n打卡地标东方明珠\r\n上海杜莎夫人蜡像馆－陆家嘴－东方明珠广播电视塔');
INSERT INTO `travel_information` VALUES ('13', '3500', '666', 'DAY1·南京\r\n《致青春》里的校园时光\r\n总统府－南京大学（鼓楼校区）－古鸡鸣寺－玄\r\n武湖公园－狮子桥美食步行街\r\nDAY2·南京\r\n中山陵缅怀国父\r\n中山陵－钟山风景区－明孝陵景区－夫子\r\n庙－秦淮风光带\r\nＤ3·南京\r\n缅怀同胞，致敬英雄\r\n侵华日军南京大屠杀遇难同胞纪念馆－雨花台风景区');
INSERT INTO `travel_information` VALUES ('14', '3500', '233', 'DAY1·上海\r\n感受摩登与古典的完美融合\r\n上海城隍庙旅游区－豫园－南京路步行街外滩\r\nDAY2·上海\r\n购物逛吃好去处\r\n中华艺术宫－田子坊－上海新天地\r\nＤ3·上海\r\n踏入“迪士尼”的童话世界\r\n上海迪士尼度假区\r\nDAY4·上海\r\n打卡地标东方明珠\r\n上海杜莎夫人蜡像馆－陆家嘴－东方明珠广播电视塔');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_account` varchar(20) NOT NULL,
  `user_password` varchar(20) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `user_state` varchar(20) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'user', '1234', '管理员', '已登录');
INSERT INTO `user` VALUES ('2', '小明', '1111', '游客', '未登录');
INSERT INTO `user` VALUES ('3', '孔名', '1234', '游客', '未登录');
INSERT INTO `user` VALUES ('4', '李华', '1234', '游客', '未登录');
INSERT INTO `user` VALUES ('5', '阿里巴巴', '1234', '游客', '未登录');
INSERT INTO `user` VALUES ('6', '妈妈咪呀', '1234', '游客', '未登录');
INSERT INTO `user` VALUES ('7', 'java', '1231', '游客', '未登录');
INSERT INTO `user` VALUES ('8', '11', '111111', '游客', '未登录');
