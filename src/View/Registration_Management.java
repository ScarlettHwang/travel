package View;
 
import java.awt.Color;
import java.awt.FileDialog;
import java.awt.Font;
 
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
 
import Bean.Tourist;
import Controller.Select;
import Controller.Updata;
import Picture.Background;

import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.awt.event.ActionEvent;
 
public class Registration_Management extends JFrame {
	
	Select select = new Select();
	Updata updata = new Updata();
	JButton btnNewButton_Export;
	JTable jTable;
	DefaultTableModel dt;
//	String name,sex,age,Idcard,address,phone,th,pt,ss,line;
	Object[] header= {"游客编号","姓名","性别","年龄","身份证号","住址","电话","旅游团","陪同","食宿","线路号"};
	Object[][] data=select.getTourist("SELECT * FROM tourist");
	private JTextField textField_ykbh;
	private JTextField textField_th;
	private JTextField textField_name;
	
	public Registration_Management() {
		super("报名信息管理");
		this.setBounds(0, 0, 1500, 900);
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
		this.setResizable(false);//让窗口大小不可改变
		getContentPane().setLayout(null);
		
		this.addWindowListener(new WindowAdapter() {
			 
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				//加入动作
				
				Background b = new Background();
				b.setVisible(true);
			 }
		});
		
		//创建表模型
		dt=new DefaultTableModel(data,header){
			//设置表格内容不可被编辑
			   public boolean isCellEditable(int row, int column) {
				    return false;//返回true表示能编辑，false表示不能编辑
				   }
		};
		jTable=new JTable(dt);//创建表格
		jTable.getTableHeader().setFont(new Font(null, Font.BOLD, 14));  // 设置表头名称字体样式
		jTable.getTableHeader().setForeground(Color.black);                // 设置表头名称字体颜色
//		jTable.getTableHeader().setResizingAllowed(false);               // 设置不允许手动改变列宽
		jTable.getTableHeader().setReorderingAllowed(false);//设置表头不允许拖动
		int v=ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;//水平滚动条
		int h=ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;//垂直滚动条
		JScrollPane jsp=new JScrollPane(jTable,v,h);//创建滚动容器
		jsp.setBounds(0, 0, 976, 675);
		getContentPane().add(jsp);
		
		JLabel label_ykbh = new JLabel("游客编号：");//游客编号
		label_ykbh.setBounds(990, 34, 89, 18);
		getContentPane().add(label_ykbh);
		
		textField_ykbh = new JTextField();
		textField_ykbh.setBounds(1066, 34, 114, 24);
		getContentPane().add(textField_ykbh);
		textField_ykbh.setColumns(10);
		
		JLabel label_th = new JLabel("团名：");
		label_th.setBounds(1000, 82, 52, 18);
		getContentPane().add(label_th);
		
		JComboBox comboBox_th = new JComboBox();
		comboBox_th.setModel(new DefaultComboBoxModel(new String[] {"夕阳红旅行团", "随心所欲旅行团",""}));
		comboBox_th.setBounds(1066, 82, 114, 24);
		getContentPane().add(comboBox_th);
		
		JLabel label_name = new JLabel("姓名：");
		label_name.setBounds(1000, 128, 52, 18);
		getContentPane().add(label_name);
		
		textField_name = new JTextField();
		textField_name.setColumns(10);
		textField_name.setBounds(1066, 128, 114, 24);
		getContentPane().add(textField_name);
		
		
		
		JButton btnNewButton_Query = new JButton("查询");
		btnNewButton_Query.setBounds(1031, 224, 113, 27);
		btnNewButton_Query.setFocusPainted(false);//去掉按钮周围的焦点框
		btnNewButton_Query.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(btnNewButton_Query);
		btnNewButton_Query.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String ykbh,th,name,sql;
				ykbh = textField_ykbh.getText();
				th = comboBox_th.getSelectedItem().toString();
				name = textField_name.getText();
				
				
				if (ykbh.equals("")&&th.equals("")&&name.equals("")) {
					sql = "SELECT * FROM tourist";                
				}else if(ykbh.equals("")&&th.equals("")){
					sql = "SELECT * FROM tourist WHERE tourist_name='"+name+"';";
				
				}else if(name.equals("")&&th.equals("")){
					sql = "SELECT * FROM tourist WHERE tourist_num='"+ykbh+"';";
				
				
				}else if(ykbh.equals("")&&name.equals("")){
					sql = "SELECT * FROM tourist WHERE group_name='"+th+"';";
				
				
				}else if(ykbh.equals("")){
					sql = "SELECT * FROM tourist WHERE group_name='"+th+"' and tourist_name='"+name+"';";
				}else if(th.equals("")){
					sql = "SELECT * FROM tourist WHERE tourist_num='"+ykbh+"' and tourist_name='"+name+"';";
				}else if(name.equals("")){
					sql = "SELECT * FROM tourist WHERE tourist_num='"+ykbh+"' and group_name='"+th+"';";
				}else {
					sql = "SELECT * FROM tourist WHERE tourist_num='"+ykbh+"' and group_name='"+th+"' and tourist_name='"+name+"';";
				}
				data = select.getTourist(sql);
				dt.setDataVector(data,header);
			}
		});
		
		JButton btnNewButton_Alter = new JButton("修改");
		btnNewButton_Alter.setBounds(1031, 278, 113, 27);
		btnNewButton_Alter.setFocusPainted(false);//去掉按钮周围的焦点框
		btnNewButton_Alter.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(btnNewButton_Alter);
		btnNewButton_Alter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (jTable.getSelectedRow()<0) {
					JOptionPane.showMessageDialog(null, "您未选中要修改的数据！");
				} else {
					//获取用户选择的数据
					String name,sex,age,Idcard,address,phone,th,pt,ss,line;
					String id=jTable.getValueAt(jTable.getSelectedRow(), 0).toString();
					String user_id = select.getString("SELECT user_id FROM `user` WHERE user_state='已登录'");
					name=jTable.getValueAt(jTable.getSelectedRow(), 1).toString();
					sex=jTable.getValueAt(jTable.getSelectedRow(), 2).toString();
					age=jTable.getValueAt(jTable.getSelectedRow(), 3).toString();
					Idcard=jTable.getValueAt(jTable.getSelectedRow(), 4).toString();
					address=jTable.getValueAt(jTable.getSelectedRow(), 5).toString();
					phone=jTable.getValueAt(jTable.getSelectedRow(), 6).toString();
					th=jTable.getValueAt(jTable.getSelectedRow(), 7).toString();
					pt=jTable.getValueAt(jTable.getSelectedRow(), 8).toString();
					ss=jTable.getValueAt(jTable.getSelectedRow(), 9).toString();
					line=jTable.getValueAt(jTable.getSelectedRow(), 10).toString();
					Tourist tourist=new Tourist(id, user_id, name, sex, age, Idcard, address, phone, th, pt, ss,line);
					RegistrationInfo_Change frame=new RegistrationInfo_Change(tourist);
					frame.setVisible(true);
				}
			}
		});
		
		JButton btnNewButton_Add = new JButton("添加");
		btnNewButton_Add.setBounds(1031, 330, 113, 27);
		btnNewButton_Add.setFocusPainted(false);//去掉按钮周围的焦点框
		btnNewButton_Add.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(btnNewButton_Add);
		btnNewButton_Add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Background b=new Background();
				b.setVisible(true);
			}
		});
		
		JButton btnNewButton_Delete = new JButton("删除");
		btnNewButton_Delete.setBounds(1031, 383, 113, 27);
		btnNewButton_Delete.setFocusPainted(false);//去掉按钮周围的焦点框
		btnNewButton_Delete.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(btnNewButton_Delete);
		btnNewButton_Delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (jTable.getSelectedRow()<0) {
					JOptionPane.showMessageDialog(null, "您未选中要删除的数据！");
				} else {
					//获取用户选择的数据
					String id=jTable.getValueAt(jTable.getSelectedRow(), 0).toString();
					String name=jTable.getValueAt(jTable.getSelectedRow(), 1).toString();
					int result = JOptionPane.showConfirmDialog(null,"您确定要删除用户  “"+name+"”  的报名信息吗?","删除提示",0,1);
					if(result == JOptionPane.OK_OPTION){
						int i = updata.addData("DELETE FROM tourist WHERE tourist_num='"+id+"';");
						if (i>0){
							JOptionPane.showMessageDialog(null, "用户  “"+name+"”  ，已被删除成功！");
						} else {
							JOptionPane.showMessageDialog(null, "删除失败！");
						}
						data=select.getTourist("SELECT * FROM tourist");
						dt.setDataVector(data,header);
					}
				}
			}
		});
		
		JButton btnNewButton_Tip = new JButton("提示");
		btnNewButton_Tip.setBounds(1031, 436, 113, 27);
		btnNewButton_Tip.setFocusPainted(false);//去掉按钮周围的焦点框
		btnNewButton_Tip.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(btnNewButton_Tip);
		btnNewButton_Tip.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "<html>查询：直接点击将列出所有报名信息，也可填写游客编号、团号和性别查询。<br>修改：点击游客将会将游客编号绑定到文本框中，可以对该游客编号对应的游客进行团号和性别修改。<br>删除：点击要删除的信息，点击删除即可。<br>添加：对报名信息进行添加。</html>");
			}
		});
		
		btnNewButton_Export = new JButton("<html>将数据导出到<br>&ensp;&ensp;Excel 表中</html>");
		
		btnNewButton_Export.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jButtonActionPerformed(e);
			}
		});
		btnNewButton_Export.setBounds(1016, 525, 138, 51);
		btnNewButton_Export.setFocusPainted(false);//去掉按钮周围的焦点框
		btnNewButton_Export.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(btnNewButton_Export);
		//设置单元格内容居中显示
		DefaultTableCellRenderer r = new DefaultTableCellRenderer();   
		r.setHorizontalAlignment(JLabel.CENTER); 
		jTable.setDefaultRenderer(Object.class, r);
		
	}
	//导出
	private void jButtonActionPerformed(java.awt.event.ActionEvent evt) {
		FileDialog fd = new FileDialog(this, "将数据保存到", FileDialog.SAVE);
		fd.setLocation(1100, 600);
		fd.setVisible(true);
		String stringfile = fd.getDirectory()+fd.getFile()+".xls";  
		try {
//		ExcelExporter export = new ExcelExporter();
			this.exportTable(jTable, new File(stringfile));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	/**导出JTable到excel */
    public void exportTable(JTable table, File file) throws IOException {
        TableModel model = table.getModel();
        BufferedWriter bWriter = new BufferedWriter(new FileWriter(file));  
        for(int i=0; i < model.getColumnCount(); i++) {
            bWriter.write(model.getColumnName(i));
            bWriter.write("\t");
        }
        bWriter.newLine();
        for(int i=0; i< model.getRowCount(); i++) {
            for(int j=0; j < model.getColumnCount(); j++) {
                bWriter.write(model.getValueAt(i,j).toString());
                bWriter.write("\t");
            }
            bWriter.newLine();
        }
        bWriter.close();
    }
	
}