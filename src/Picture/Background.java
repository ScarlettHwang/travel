package Picture;


	import java.awt.*;
	import javax.swing.*;

import View.MainJframe;
	public class Background extends MainJframe{
		ImageIcon background;
		JPanel myPanel;
		JPanel contentPane;
		JLabel label;
	
		
		public Background()
		{
			super();
		
			this.setBounds(0, 0, 1200, 700);
			this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
			this.setResizable(false);//让窗口大小不可改变
			getContentPane().setLayout(null);
			myPanel=new JPanel();
			getContentPane().add(myPanel);
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			
			background = new ImageIcon("img/java.png");	//创建一个背景图片
			label = new JLabel(background);		//把背景图片添加到标签里
			this.getLayeredPane().add(label, new Integer(Integer.MIN_VALUE));		//把标签添加到分层面板的最底层
			//设置界面属性
			label.setBounds(0, 0, 1200,700);	//把标签设置为和图片等高等宽
			myPanel = (JPanel)this.getContentPane();		//把我的面板设置为内容面板
			((JComponent) getContentPane()).setOpaque(false);
								//把我的面板设置为不可视
			myPanel.add(label);
			
			this.getLayeredPane().setLayout(null);		//把分层面板的布局置空
			
			
	       

			
		}
		public static void main(String[] args) {
			Background b = new Background();
			b.setVisible(true);
		}
	}


