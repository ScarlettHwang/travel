package View;
import java.awt.*;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.Timer;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import org.junit.Test;

import Controller.Select;
import Controller.Updata;
import Picture.Background;
import Utils.ValidateUtils;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
 
public class MainJframe extends JFrame {
	private JTextField textField_name;//姓名
	private JTextField textField_age;//年龄
	private JTextField textField_IDcard;//身份证号码
	private JTextField textField_address;//地址
	private JTextField textField_phone;//电话

	private JTextField textField_pt;//陪同
	private JTextField textField_ss;//食宿
	private JTextField textField_line;//线路号
	String name,sex,age,Idcard,address,phone,th,pt,ss,line;
 
	Select select = new Select();
	Updata updata = new Updata();
	Object[] header= {"线路号","起点","终点","天数","主要景点","团号"};
	Object[][] data=select.getLineInfo();
	
	public MainJframe() {
		super("旅游管理信息系统");
		this.setBounds(0, 0, 1200, 700);
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
		this.setResizable(false);//让窗口大小不可改变
		getContentPane().setLayout(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//用户单击窗口的关闭按钮时程序执行的操作
		
		JLabel lblNewLabel_name= new JLabel("姓名：");
		lblNewLabel_name.setBounds(100, 300, 72, 18);
		getContentPane().add(lblNewLabel_name);
		
		textField_name = new JTextField();
		textField_name.setBounds(150, 295, 180, 24);
		getContentPane().add(textField_name);
		textField_name.setColumns(10);
		
		JLabel lblNewLabel_sex= new JLabel("性别：");
		lblNewLabel_sex.setBounds(100, 350, 72, 18);
		getContentPane().add(lblNewLabel_sex);
		JComboBox comboBox_sez = new JComboBox();
		comboBox_sez.setModel(new DefaultComboBoxModel(new String[] {"男", "女"}));
		comboBox_sez.setBounds(150, 345, 180, 24);
		getContentPane().add(comboBox_sez);
		
		JLabel lblNewLabel_age= new JLabel("年龄：");
		lblNewLabel_age.setBounds(100, 400, 72, 18);
		getContentPane().add(lblNewLabel_age);
		
		textField_age = new JTextField();
		textField_age.setBounds(150,395 , 180, 24);
		getContentPane().add(textField_age);
		textField_age.setColumns(10);
		
		JLabel lblNewLabel_IDcard= new JLabel("身份证号码:");
		lblNewLabel_IDcard.setBounds(65, 450, 72, 18);
		getContentPane().add(lblNewLabel_IDcard);
		
		textField_IDcard = new JTextField();
		textField_IDcard.setBounds(150, 445, 180, 24);
		getContentPane().add(textField_IDcard);
		textField_IDcard.setColumns(10);
		
		JLabel lblNewLabel_address= new JLabel("住址：");
		lblNewLabel_address.setBounds(100, 500, 72, 18);
		getContentPane().add(lblNewLabel_address);
		
		textField_address = new JTextField();
		textField_address.setBounds(150, 495, 180, 24);
		getContentPane().add(textField_address);
		textField_address.setColumns(10);
		
		JLabel lblNewLabel_phone= new JLabel("电话：");
		lblNewLabel_phone.setBounds(100, 550, 72, 18);
		getContentPane().add(lblNewLabel_phone);
		
		textField_phone = new JTextField();
		textField_phone.setBounds(150, 545, 180, 24);
		getContentPane().add(textField_phone);
		textField_phone.setColumns(10);
		
		JLabel lblNewLabel_th= new JLabel("团名：");
		lblNewLabel_th.setBounds(500, 300, 72, 18);
		getContentPane().add(lblNewLabel_th);
		
		
		JComboBox comboBox_th = new JComboBox();
		comboBox_th.setModel(new DefaultComboBoxModel(new String[] {"夕阳红旅行团", "随心所欲旅行团"}));
		comboBox_th.setBounds(550, 295, 180, 24);
		getContentPane().add(comboBox_th);
		
		
		
		ImageIcon i0 = new ImageIcon("img/Icon0.png");
		JButton lvt = new JButton("查看旅游团信息",i0);
		lvt.setBounds(780, 290, 158, 27);
		lvt.setFocusPainted(false);//去掉按钮周围的焦点框
		lvt.setContentAreaFilled(true);//设置按钮bu透明背景
		getContentPane().add(lvt);
		lvt.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent arg0) {		
				String sql = "SELECT COUNT(*) FROM `user` WHERE user_state='已登录'";
				int reselt = select.getCount(sql);
				if (reselt>0) {
					//判断当前登录的用户身份
					
						Enquiry e = new Enquiry();
						e.setVisible(true);
						//dispose();
				}
					 else {
					JOptionPane.showMessageDialog(null, "请先登录！");
					Login l = new Login();
					l.setVisible(true);
					dispose();
				}
			}
		});

		
		JLabel lblNewLabel_line= new JLabel("线路号：");
		lblNewLabel_line.setBounds(500, 450, 72, 18);
		getContentPane().add(lblNewLabel_line);
		
		textField_line = new JTextField();
		textField_line.setBounds(550, 445, 180, 24);
		getContentPane().add(textField_line);
		textField_line.setColumns(10);
		
		JLabel lblNewLabel_pt= new JLabel("陪同：");
		lblNewLabel_pt.setBounds(500, 350, 72, 18);
		getContentPane().add(lblNewLabel_pt);
		
		textField_pt = new JTextField();
		textField_pt.setText("是否选择导游陪同？");
		textField_pt.setToolTipText("");
		textField_pt.setBounds(550, 345, 180, 24);
		getContentPane().add(textField_pt);
		textField_pt.setColumns(10);
		
		JLabel lblNewLabel_ss= new JLabel("食宿：");
		lblNewLabel_ss.setBounds(500, 400, 72, 18);
		getContentPane().add(lblNewLabel_ss);
		
		textField_ss = new JTextField();
		textField_ss.setText("是否选择宾馆住宿？");
		textField_ss.setBounds(550, 395, 180, 24);
		getContentPane().add(textField_ss);
		textField_ss.setColumns(10);
		
		JButton button_3 = new JButton("是");
		button_3.setBounds(780, 340, 50, 27);
		button_3.setFocusPainted(false);//去掉按钮周围的焦点框
		button_3.setContentAreaFilled(true);//设置按钮透明背景
		getContentPane().add(button_3);
		button_3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				textField_pt.setText("是");
			}
		});
		
		JButton button_4 = new JButton("否");
		button_4.setBounds(850, 340, 50, 27);
		button_4.setFocusPainted(false);//去掉按钮周围的焦点框
		button_4.setContentAreaFilled(true);//设置按钮不透明背景
		getContentPane().add(button_4);
		button_4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				textField_pt.setText("无");
			}
		});
		
		JButton button_6 = new JButton("是");
		button_6.setBounds(780, 390, 50, 27);
		button_6.setFocusPainted(false);//去掉按钮周围的焦点框
		button_6.setContentAreaFilled(true);//设置按钮透明背景
		getContentPane().add(button_6);
		button_6.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				textField_ss.setText("是");
			}
		});
		
		JButton button_7 = new JButton("否");
		button_7.setBounds(850, 390, 50, 27);
		button_7.setFocusPainted(false);//去掉按钮周围的焦点框
		button_7.setContentAreaFilled(true);//设置按钮透明背景
		getContentPane().add(button_7);
		button_7.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				textField_ss.setText("无");
			}
		});
		
		
		
		JButton btnNewButton = new JButton("报名");
		btnNewButton.setFont(new Font("微软雅黑", Font.PLAIN, 18));
		btnNewButton.setBounds(700, 516, 124, 33);
		btnNewButton.setFocusPainted(false);//去掉按钮周围的焦点框
		btnNewButton.setContentAreaFilled(true);//设置按钮不透明背景
		getContentPane().add(btnNewButton);
		
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				
				name = textField_name.getText();
				sex = comboBox_sez.getSelectedItem().toString();
				age = textField_age.getText();
				Idcard = textField_IDcard.getText();
				address = textField_address.getText();
				phone = textField_phone.getText();
				line = textField_line.getText();
				th = comboBox_th.getSelectedItem().toString();
				
				pt = textField_pt.getText();
			
				if (pt.equals("是否选择导游陪同？")) {
					pt="无";
				}
				ss = textField_ss.getText();
				if (ss.equals("是否选择宾馆住宿？")) {
					ss="无";
				}
				//判断输入的信息是否为空，是否完整
				if (name.equals("")||sex.equals("")||age.equals("")||Idcard.equals("")||address.equals("")||phone.equals("")||th.equals("")||pt.equals("")||ss.equals("")||line.equals("")) {
					JOptionPane.showMessageDialog(null, "请输入完整信息！");
				} else {
					//判断身份证号码
					if (!ValidateUtils.IDcard(Idcard)) {
						JOptionPane.showMessageDialog(null, "身份证号码错误！请检查！");
					} else {
						String i = select.getString("SELECT user_id FROM `user` WHERE user_state='已登录'");
						String sql = "INSERT INTO `tourist` VALUES (null, '"+i+"', '"+name+"', '"+sex+"', '"+age+"', '"+Idcard+"', '"+address+"', '"+phone+"', '"+th+"', '"+pt+"', '"+ss+"','"+line+"');";
						int result = updata.addData(sql);
						//判断手机号
						String regex = "^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(17[013678])|(18[0,5-9]))\\d{8}$";
				        if(phone.length() != 11){
				        	JOptionPane.showMessageDialog(null, "手机号应为11位数！");
				        }else{
				            Pattern p = Pattern.compile(regex);
				            Matcher m = p.matcher(phone);
				            boolean isMatch = m.matches();
				            if(!isMatch){
				                JOptionPane.showMessageDialog(null, "您的手机号" + phone + "是错误格式！！！");
				            }else {
				            	//判断插入结果
				            	if (result>0) {
				            		JOptionPane.showMessageDialog(null, "报名成功！");
				            		
				            		dispose();
				            		
				        					
				        					Background b = new Background();
				        					b.setVisible(true);
				        				 
				            	} else {
				            		JOptionPane.showMessageDialog(null, "报名失败，请与管理员联系！");
				            	}
							}
				        }
					}
				}
			}
		});
//按钮
		ImageIcon i1 = new ImageIcon("img/Icon1.png");
		JButton enq = new JButton("旅游信息查询",i1);
		enq.setBounds(14, 11, 145, 35);
		enq.setFocusPainted(false);//去掉按钮周围的焦点框
		enq.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(enq);
		enq.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent arg0) {
				trval_info t=new trval_info();
				t.setVisible(true);
				//dispose();
			}
		});

		
		ImageIcon i2 = new ImageIcon("img/Icon2.png");
		JButton ywgl = new JButton("业务管理员",i2);
		ywgl.setBounds(165, 11, 130, 35);
		ywgl.setFocusPainted(false);//去掉按钮周围的焦点框
		ywgl.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(ywgl);
		ywgl.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//判断当前是否有用户登录
				String sql = "SELECT COUNT(*) FROM `user` WHERE user_state='已登录'";
				int reselt = select.getCount(sql);
				if (reselt>0) {
					//判断当前登录的用户身份
					String user_type = select.getString("SELECT user_type FROM `user` WHERE user_state='已登录'");
					if (user_type.equals("管理员")) {
						Registration_Management r = new Registration_Management();
						r.setVisible(true);
						dispose();
					}else{
						JOptionPane.showMessageDialog(null, "当前无权限！请登录管理员账号！");
					}
				} else {
					JOptionPane.showMessageDialog(null, "请先登录！");
					Login l = new Login();
					l.setVisible(true);
					dispose();
				}
			}
		});
		
		ImageIcon i3 = new ImageIcon("img/Icon3.png");
		JButton yhdl = new JButton("登录",i3);
		yhdl.setBounds(300, 11, 130, 35);
		yhdl.setFocusPainted(false);//去掉按钮周围的焦点框
		yhdl.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(yhdl);
		yhdl.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//判断当前是否有用户登录
				String sql = "SELECT COUNT(*) FROM `user` WHERE user_state='已登录'";
				int reselt = select.getCount(sql);
				if (reselt>0) {
					String i = select.getString("SELECT user_id FROM `user` WHERE user_state='已登录'");
					JOptionPane.showMessageDialog(null, "当前已有ID为"+"   ”"+i+"”   "+"的用户登录");
				}else {
					Login l = new Login();
					l.setVisible(true);
					dispose();
				}
			}
		});
		
		ImageIcon i4 = new ImageIcon("img/Icon4.png");
		JButton yhzc = new JButton("注册",i4);
		yhzc.setBounds(435, 11, 130, 35);
		yhzc.setFocusPainted(false);//去掉按钮周围的焦点框
		yhzc.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(yhzc);
		yhzc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//判断当前是否有用户登录
				String sql = "SELECT COUNT(*) FROM `user` WHERE user_state='已登录'";
				int reselt = select.getCount(sql);
				if (reselt>0) {
					String i = select.getString("SELECT user_account FROM `user` WHERE user_state='已登录'");
//					JOptionPane.showMessageDialog(null, "当前已有用户"+"   ”"+i+"”   "+"登录！是否注销？");
					int a = JOptionPane.showConfirmDialog(null,"当前已有用户"+"   ”"+i+"”   "+"登录！是否注销？","注销提示",0,1);
					if(a == JOptionPane.OK_OPTION){
						JOptionPane.showMessageDialog(null, "已注销前账户！");
						updata.addData("UPDATE user SET user_state='未登录' WHERE user_account='"+i+"';");
						Registered r = new Registered();
						r.setVisible(true);
						dispose();
	                }
				}else {
					Registered r = new Registered();
					r.setVisible(true);
					dispose();
				}
			}
		});
		
		ImageIcon i5 = new ImageIcon("img/Icon5.png");
		JButton tcxt = new JButton("退出系统",i5);
		tcxt.setBounds(570, 11, 130, 35);
		tcxt.setFocusPainted(false);//去掉按钮周围的焦点框
		tcxt.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(tcxt);
		tcxt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int result = JOptionPane.showConfirmDialog(null,"您现在要关闭系统吗?关闭后同时注销账号!","退出提示",0,1);
				if(result == JOptionPane.OK_OPTION){
					JOptionPane.showMessageDialog(null, "已退出系统，欢迎下次使用！");
					updata.addData("UPDATE user SET user_state='未登录';");
                    
                }
			}
		});
		
		ImageIcon i6 = new ImageIcon("img/Icon6.png");
		JButton help = new JButton("帮助",i6);
		help.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "系统管理员电话：4208208820");
			}
		});
		help.setBounds(705, 11, 130, 35);
		help.setFocusPainted(false);//去掉按钮周围的焦点框
		help.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(help);
		
		JLabel dqsj = new JLabel("当前时间 ：");
//		dqsj.setForeground(Color.decode("#7784BD"));
		dqsj.setBounds(857, 13, 85, 35);
		dqsj.setFont(new Font("微软雅黑", Font.BOLD, 15));
		getContentPane().add(dqsj);
		
		JLabel time1 = new JLabel();
//		time1.setForeground(Color.decode("#7784BD"));
		time1.setBounds(944, 14, 236, 35);
		time1.setFont(new Font("宋体", Font.CENTER_BASELINE, 15));
		getContentPane().add(time1);
		this.setTimer(time1);
		
		//创建表模型
		DefaultTableModel dt=new DefaultTableModel(data,header){
			//设置表格内容不可被编辑
			   public boolean isCellEditable(int row, int column) {
				    return false;//返回true表示能编辑，false表示不能编辑
				   }
		};
		JTable jTable=new JTable(dt);//创建表格
		jTable.getTableHeader().setFont(new Font(null, Font.BOLD, 14));  // 设置表头名称字体样式
		jTable.getTableHeader().setForeground(Color.black);                // 设置表头名称字体颜色
		jTable.getTableHeader().setResizingAllowed(false);               // 设置不允许手动改变列宽
		jTable.getTableHeader().setReorderingAllowed(false);//设置表头不允许拖动
		int v=ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;//水平滚动条
		int h=ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;//垂直滚动条
		JScrollPane jsp=new JScrollPane(jTable,v,h);//创建滚动容器
		jsp.setBounds(14, 68, 1166, 200);
		getContentPane().add(jsp);
		//设置单元格内容居中显示
		DefaultTableCellRenderer r = new DefaultTableCellRenderer();   
		r.setHorizontalAlignment(JLabel.CENTER); 
		jTable.setDefaultRenderer(Object.class, r);
		
	}
	
	// 设置Timer 1000ms实现一次动作 实际是一个线程
	private void setTimer(JLabel time) {
		final JLabel varTime = time;
		Timer timeAction = new Timer(100, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				long timemillis = System.currentTimeMillis();
				// 转换日期显示格式
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				varTime.setText(df.format(new Date(timemillis)));
			}
		});
		timeAction.start();
	}
	
	public static void main(String[] args) {
		
		Background b = new Background();
		b.setVisible(true);
	}
}