package View;
 
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
 
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import Controller.Select;
import Controller.Updata;
import Picture.Background;

import javax.swing.JButton;
 
public class Login extends JFrame {
	Select select = new Select();
	Updata updata = new Updata();
	private JTextField textField_zh;
	private JPasswordField textField_mm;
	
	public Login() { 
		super.setTitle("系统登录");
		this.setBounds(0, 0, 700, 550);//设置大小
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
  	    this.setResizable(false);//让窗口大小不可改变
		getContentPane().setLayout(null);
		this.getContentPane().setBackground(new Color(176, 211, 238, 207));
		
		JLabel label_zh = new JLabel("ID号：");
		label_zh.setFont(new Font("宋体", Font.CENTER_BASELINE, 15));
		label_zh.setBounds(183, 135, 72, 18);
		getContentPane().add(label_zh);
		
		textField_zh = new JTextField();
		textField_zh.setBounds(233, 130, 270, 34);
		getContentPane().add(textField_zh);
		textField_zh.setColumns(10);
		
		JLabel label_mm = new JLabel("密码：");
		label_mm.setFont(new Font("宋体", Font.CENTER_BASELINE, 15));
		label_mm.setBounds(183, 205, 72, 18);
		getContentPane().add(label_mm);
		textField_mm = new JPasswordField();
		textField_mm.setBounds(233, 200, 270, 34);
		getContentPane().add(textField_mm);
		textField_mm.setColumns(10);

		JButton button1 = new JButton("用户登录");
		button1.setFont(new Font("宋体", Font.BOLD, 20));
		button1.setBounds(150, 299, 180, 34);
		button1.setFocusPainted(false);//去掉按钮周围的焦点框
		//button1.setContentAreaFilled(false);//设置按钮透明背景
		button1.setBackground(new Color(127, 155, 180));
		getContentPane().add(button1);
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String zh=textField_zh.getText();
				String password=textField_mm.getText();
				if (zh.equals("")&&password.equals("")) {
					JOptionPane.showMessageDialog(null, "账户名或密码未填写！");
				} else {
					String sql = "select COUNT(*) from user where user_password='"+password+"' and user_id='"+zh+"'";
					int reselt = select.getCount(sql);
					if (reselt<=0) {
						JOptionPane.showMessageDialog(null, "登录失败！账户名或密码不正确！请重新输入！");
					} else {
						String sql1 = "select COUNT(*) from user where user_password='" + password + "' and user_id='" + zh + "' and user_type='管理员'";
						int reselt1 = select.getCount(sql1);
						if (reselt1 > 0) {
							JOptionPane.showMessageDialog(null, "非游客身份！请点击管理员登录！");
						} else {
							updata.addData("UPDATE user SET user_state='已登录' WHERE user_id='" + zh + "';");
							Background m = new Background();
							m.setVisible(true);
							dispose();
						}
					}
				}
			}

		});

		JButton button = new JButton("管理员登录");
		button.setFont(new Font("宋体", Font.BOLD, 20));
		button.setBounds(350, 299, 180, 34);
		button.setFocusPainted(false);//去掉按钮周围的焦点框
		//button.setContentAreaFilled(false);//设置按钮透明背景
		button.setBackground(new Color(127, 155, 180));
		getContentPane().add(button);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String zh=textField_zh.getText();
				String password=textField_mm.getText();
				if (zh.equals("")&&password.equals("")) {
					JOptionPane.showMessageDialog(null, "账户名或密码未填写！");
				} else {
					String sql = "select COUNT(*) from user where user_password='"+password+"' and user_id='"+zh+"'";
					int reselt = select.getCount(sql);
					if (reselt<=0) {
						JOptionPane.showMessageDialog(null, "登录失败！账户名或密码不正确！请重新输入！");
					} else {
						String sql1 = "select COUNT(*) from user where user_password='" + password + "' and user_id='" + zh + "' and user_type='游客'";
						int reselt1 = select.getCount(sql1);
						if (reselt1 > 0) {
							JOptionPane.showMessageDialog(null, "非管理员身份！请点击用户登录！");
						} else {
							updata.addData("UPDATE user SET user_state='已登录' WHERE user_id='" + zh + "';");
							Background m = new Background();
							m.setVisible(true);
							dispose();
						}
					}
				}
			}
		});
		JLabel ljzc = new JLabel("没有账号？立即注册!");
		ljzc.setFont(new Font("宋体", Font.ITALIC, 16));
		ljzc.setForeground(Color.blue);
		ljzc.setBounds(271, 380, 168, 27);
		getContentPane().add(ljzc);
		ljzc.addMouseListener(new MouseListener(){
			public void mouseClicked(MouseEvent e) {
				// 处理鼠标点击
				Registered m = new Registered();
				m.setVisible(true);
				//dispose();
			}
			public void mouseEntered(MouseEvent e) {
				// 处理鼠标移入
				ljzc.setForeground(Color.red);
			}
			public void mouseExited(MouseEvent e) {
				// 处理鼠标离开
				ljzc.setForeground(Color.blue);
			}
			public void mousePressed(MouseEvent e) {
				// 处理鼠标按下
			}
			public void mouseReleased(MouseEvent e) {
				// 处理鼠标释放
			}
		});
		
		this.addWindowListener(new WindowAdapter() {
			 
			public void windowClosing(WindowEvent e) {
				
				Background b = new Background();
				b.setVisible(true);
			 }
		});
		
	}
}