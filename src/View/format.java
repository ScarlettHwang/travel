package View;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.*;

public class format  extends JTextArea implements TableCellRenderer {

    public format() {

        setLineWrap(true);//激活自动换行
        //setWrapStyleWord(true);//断行不断字

    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        // 计算当下行的最佳高度
        int maxPreferredHeight = 25;
        for (int i = 0; i < table.getColumnCount(); i++) {
            setText("" + table.getValueAt(row, i));
            setSize(table.getColumnModel().getColumn(column).getWidth(), 0);
            maxPreferredHeight = Math.max(maxPreferredHeight, getPreferredSize().height);
        }

        if (table.getRowHeight(row) != maxPreferredHeight){ // 少了这行则处理器瞎忙
            table.setRowHeight(row, maxPreferredHeight); }

        if (isSelected ) {
            this.setForeground(Color.red);
        }else {
            this.setForeground(Color.BLACK);
            this.setBackground(table.getBackground());
        }

       /* TableColumnModel tcm=table.getColumnModel();
        TableColumn tc=tcm.getColumn(1);
        tc.setWidth(0);
        tc.setPreferredWidth(0);
        tc.setMaxWidth(0);
        tc.setMinWidth(0);
        table.getTableHeader().getColumnModel().getColumn(1)
                .setMaxWidth(0);
        table.getTableHeader().getColumnModel().getColumn(1)
                .setMinWidth(0);*/

        //设置列宽
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.getColumnModel().getColumn(0).setPreferredWidth(80);
        table.getColumnModel().getColumn(2).setPreferredWidth(95);
        table.getColumnModel().getColumn(3).setPreferredWidth(415);
        table.getColumnModel().getColumn(4).setPreferredWidth(180);
        table.getColumnModel().getColumn(5).setPreferredWidth(100);
        table.getColumnModel().getColumn(6).setPreferredWidth(190);
        table.getColumnModel().getColumn(7).setPreferredWidth(136);
        table.getColumnModel().getColumn(8).setPreferredWidth(100);
        table.getColumnModel().getColumn(9).setPreferredWidth(100);
        table.getColumnModel().getColumn(10).setPreferredWidth(95);
        setText(value == null ? "" : value.toString());
        return this;
    }

}
