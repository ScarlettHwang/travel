package Bean;

public class Tourism_information {
    String group_num;//团号
    String fee;//费用
    String detail;//详情介绍
     String route_num;//路线号
public  Tourism_information(){super();}

    public String getDetail() {
        return detail;
    }

    public String getFee() {
        return fee;
    }

    public String getGroup_num() {
        return group_num;
    }

    public String getRoute_num() {
        return route_num;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public void setGroup_num(String group_num) {
        this.group_num = group_num;
    }

    public void setRoute_num(String route_num) {
        this.route_num = route_num;
    }

    @Override
    public String toString() {
        return "Tourism_information{" +
                "group_num='" + group_num + '\'' +
                ", fee='" + fee + '\'' +
                ", detail='" + detail + '\'' +
                ", route_num='" + route_num + '\'' +
                '}';
    }
    public Tourism_information(String group_num,String fee,String detail,String route_num)
    {
        this.group_num=group_num;
        this.fee=fee;
        this.detail=detail;
        this.route_num=route_num;
    }
}
