package View;
 
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
 
import Bean.Tourist;
import Controller.Select;
import Controller.Updata;
import Utils.ValidateUtils;
 
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
 
public class RegistrationInfo_Change extends JFrame {
	private JTextField textField_name;//姓名
	private JTextField textField_age;//年龄
	private JTextField textField_IDcard;//身份证号码
	private JTextField textField_address;//地址
	private JTextField textField_phone;//电话
	private JTextField textField_line;//线路号
	
	private JTextField textField_pt;//陪同
	private JTextField textField_ss;//食宿
	
	Select select = new Select();
	Updata updata = new Updata();
	String name,sex,age,Idcard,address,phone,th,pt,ss,line;
 
	public RegistrationInfo_Change(Tourist tourist) {
		super("修改报名信息");
		this.setBounds(0, 0, 930, 700);
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
		this.setResizable(false);//让窗口大小不可改变
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel_name= new JLabel("姓名：");
		lblNewLabel_name.setBounds(138, 79, 72, 18);
		getContentPane().add(lblNewLabel_name);
		
		textField_name = new JTextField();
		textField_name.setBounds(191, 76, 240, 24);
		getContentPane().add(textField_name);
		textField_name.setColumns(10);
		
		JLabel lblNewLabel_sex= new JLabel("性别：");
		lblNewLabel_sex.setBounds(138, 125, 72, 18);
		getContentPane().add(lblNewLabel_sex);
		
		JComboBox comboBox_sex= new JComboBox();
		comboBox_sex.setModel(new DefaultComboBoxModel(new String[] {"男", "女"}));
		comboBox_sex.setBounds(191, 122, 240, 24);
		getContentPane().add(comboBox_sex);
		
		JLabel lblNewLabel_age= new JLabel("年龄：");
		lblNewLabel_age.setBounds(138, 168, 72, 18);
		getContentPane().add(lblNewLabel_age);
		
		textField_age = new JTextField();
		textField_age.setBounds(191, 165, 240, 24);
		getContentPane().add(textField_age);
		textField_age.setColumns(10);
		
		JLabel lblNewLabel_IDcard= new JLabel("身份证号码：");
		lblNewLabel_IDcard.setBounds(93, 213, 117, 18);
		getContentPane().add(lblNewLabel_IDcard);
		
		textField_IDcard = new JTextField();
		textField_IDcard.setBounds(191, 210, 240, 24);
		getContentPane().add(textField_IDcard);
		textField_IDcard.setColumns(10);
		
		JLabel lblNewLabel_address= new JLabel("住址：");
		lblNewLabel_address.setBounds(138, 254, 72, 18);
		getContentPane().add(lblNewLabel_address);
		
		textField_address = new JTextField();
		textField_address.setBounds(191, 251, 240, 24);
		getContentPane().add(textField_address);
		textField_address.setColumns(10);
		
		JLabel lblNewLabel_phone= new JLabel("电话：");
		lblNewLabel_phone.setBounds(138, 295, 72, 18);
		getContentPane().add(lblNewLabel_phone);
		textField_phone = new JTextField();
		textField_phone.setBounds(191, 292, 240, 24);
		getContentPane().add(textField_phone);
		textField_phone.setColumns(10);
		
		JLabel lblNewLabel_line= new JLabel("线路号：");
		lblNewLabel_line.setBounds(138, 472, 72, 18);
		getContentPane().add(lblNewLabel_line);
		textField_line = new JTextField();
		textField_line.setBounds(191, 469, 240, 24);
		getContentPane().add(textField_line);
		textField_line.setColumns(10);
		
		
		JLabel lblNewLabel_th= new JLabel("团名：");
		lblNewLabel_th.setBounds(138, 335, 72, 18);
		getContentPane().add(lblNewLabel_th);
		
		@SuppressWarnings("rawtypes")
		JComboBox comboBox_th = new JComboBox();
		comboBox_th.setModel(new DefaultComboBoxModel(new String[] {"夕阳红旅行团", "随心所欲旅行团"}));
		comboBox_th.setBounds(191, 332, 240, 24);
		getContentPane().add(comboBox_th);
		
		JLabel lblNewLabel_pt= new JLabel("陪同：");
		lblNewLabel_pt.setBounds(138, 382, 72, 18);
		getContentPane().add(lblNewLabel_pt);
		
		textField_pt = new JTextField();
		textField_pt.setText("是否选择导游陪同？");
		textField_pt.setToolTipText("");
		textField_pt.setBounds(191, 379, 240, 24);
		getContentPane().add(textField_pt);
		textField_pt.setColumns(10);
		
		JLabel lblNewLabel_ss= new JLabel("食宿：");
		lblNewLabel_ss.setBounds(138, 427, 72, 18);
		getContentPane().add(lblNewLabel_ss);
		
		textField_ss = new JTextField();
		textField_ss.setText("是否选择宾馆住宿？");
		textField_ss.setBounds(191, 424, 240, 24);
		getContentPane().add(textField_ss);
		textField_ss.setColumns(10);
		
		JButton button_1 = new JButton("查看旅游团信息");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "<html>夕阳红旅行社：团号-666 <br>随心所欲旅行社：团号-233 ");
			}
		});
		button_1.setBounds(453, 331, 158, 27);
		button_1.setFocusPainted(false);//去掉按钮周围的焦点框
		button_1.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(button_1);
		
		JButton button_2 = new JButton("显示选择结果");
		button_2.setBounds(625, 331, 146, 27);
		button_2.setFocusPainted(false);//去掉按钮周围的焦点框
		button_2.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(button_2);
		
		JButton button_3 = new JButton("是");
		button_3.setBounds(453, 378, 72, 27);
		button_3.setFocusPainted(false);//去掉按钮周围的焦点框
		button_3.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(button_3);
		button_3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				textField_pt.setText("是");
			}
		});
		
		JButton button_4 = new JButton("否");
		button_4.setBounds(539, 378, 72, 27);
		button_4.setFocusPainted(false);//去掉按钮周围的焦点框
		button_4.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(button_4);
		button_4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				textField_pt.setText("无");
			}
		});
		
		JButton button_5 = new JButton("显示选择结果");
		button_5.setBounds(625, 378, 146, 27);
		button_5.setFocusPainted(false);//去掉按钮周围的焦点框
		button_5.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(button_5);
		
		JButton button_6 = new JButton("是");
		button_6.setBounds(453, 421, 72, 27);
		button_6.setFocusPainted(false);//去掉按钮周围的焦点框
		button_6.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(button_6);
		button_6.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				textField_ss.setText("是");
			}
		});
		
		JButton button_7 = new JButton("否");
		button_7.setBounds(539, 421, 72, 27);
		button_7.setFocusPainted(false);//去掉按钮周围的焦点框
		button_7.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(button_7);
		button_7.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				textField_ss.setText("无");
			}
		});
		
		JButton button_8 = new JButton("显示选择结果");
		button_8.setBounds(625, 423, 146, 27);
		button_8.setFocusPainted(false);//去掉按钮周围的焦点框
		button_8.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(button_8);
		
		textField_name.setText(tourist.getTourist_name());
		String t = tourist.getTourist_sex();
		comboBox_sex.setSelectedItem(t);
		textField_age.setText(tourist.getTourist_age());
		textField_IDcard.setText(tourist.getTourist_idCard());
		textField_address.setText(tourist.getTourist_address());
		textField_phone.setText(tourist.getTourist_phone());
		textField_line.setText(tourist.getRoute_num());
		String s=tourist.getGroup_name();
		comboBox_th.setSelectedItem(s);
		textField_pt.setText(tourist.getAccompanied());
		textField_ss.setText(tourist.getAccommodation());
		
		JButton btnNewButton = new JButton("确认修改");
		btnNewButton.setFont(new Font("微软雅黑", Font.PLAIN, 18));
		btnNewButton.setBounds(388, 516, 124, 33);
		btnNewButton.setFocusPainted(false);//去掉按钮周围的焦点框
		btnNewButton.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String id = tourist.getTourist_num();
				name = textField_name.getText();
				sex = comboBox_sex.getSelectedItem().toString();
				age = textField_age.getText();
				Idcard = textField_IDcard.getText();
				address = textField_address.getText();
				phone = textField_phone.getText();
				th = comboBox_th.getSelectedItem().toString();
				line= textField_line.getText();
				pt = textField_pt.getText();
				if (pt.equals("是否选择导游陪同？")) {
					pt="无";
				}
				ss = textField_ss.getText();
				if (ss.equals("是否选择宾馆住宿？")) {
					ss="无";
				}
				//判断输入的信息是否为空，是否完整
				if (name.equals("")||sex.equals("")||age.equals("")||Idcard.equals("")||address.equals("")||phone.equals("")||th.equals("")||pt.equals("")||ss.equals("")||line.equals("")) {
					JOptionPane.showMessageDialog(null, "请输入完整信息！");
				} else {
					//判断身份证号码
					if (!ValidateUtils.IDcard(Idcard)) {
						JOptionPane.showMessageDialog(null, "身份证号码错误！请检查！");
					} else {
						String i = select.getString("SELECT user_id FROM `user` WHERE user_state='已登录'");
						String sql = "UPDATE tourist SET tourist_name='"+name+"',tourist_sex='"+sex+"',tourist_age='"+age+"',tourist_idcard='"+Idcard+"',tourist_address='"+address+"',tourist_phone='"+phone+"',group_name='"+th+"',accompanied='"+pt+"',accommodation='"+ss+"',route_num='"+line+"' WHERE tourist_num='"+id+"';";
						int result = updata.addData(sql);
						//判断手机号
						String regex = "^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(17[013678])|(18[0,5-9]))\\d{8}$";
				        if(phone.length() != 11){
				        	JOptionPane.showMessageDialog(null, "手机号应为11位数！");
				        }else{
				            Pattern p = Pattern.compile(regex);
				            Matcher m = p.matcher(phone);
				            boolean isMatch = m.matches();
				            if(!isMatch){
				                JOptionPane.showMessageDialog(null, "您的手机号" + phone + "是错误格式！！！");
				            }else {
				            	//判断插入结果
				            	if (result>0) {
				            		JOptionPane.showMessageDialog(null, "修改成功！");
				            		Registration_Management r = new Registration_Management();
									r.dispose();
									r.setVisible(true);
									dispose();
				            	} else {
				            		JOptionPane.showMessageDialog(null, "修改失败，请与管理员联系！");
				            	}
							}
				        }
					}
				}
			}
		});
		
	}
 
}