package View;

import Controller.Select;
import Picture.Background;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;
import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.*;

//按路线号查询界面
//在表格内添加按钮
//一条路线信息表格显示
public class trval_info extends JFrame{

    private JButton submitQuery;
    private JButton nextQuery;

    public trval_info() {
        //查询窗体
        super("旅游信息查询");

        this.setBounds(0, 0, 600, 260);
        this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
        this.setResizable(false);//让窗口大小不可改变
        getContentPane().setLayout(null);
        this.getContentPane().setBackground(new Color(163, 195, 208));

        JLabel lblNewLabel_name= new JLabel("路线号：");
        lblNewLabel_name.setBounds(138, 79, 72, 18);
        getContentPane().add(lblNewLabel_name);
        JTextField textField_route = new JTextField();
        textField_route.setBounds(191, 76, 240, 24);
        getContentPane().add(textField_route);
        textField_route.setColumns(10);

        //查询按钮
        submitQuery = new JButton( "查询" );
        submitQuery.setBounds(230, 120, 120, 27);
        submitQuery.setFocusPainted(false);//去掉按钮周围的焦点框
        submitQuery.setContentAreaFilled(false);//设置按钮透明背景
        getContentPane().add(submitQuery);

        submitQuery.addActionListener(new ActionListener() {
            Vector rowData,columnNames;
            JTable jt=null;
            JScrollPane jsp=null;
            Connection conn=null;
            ResultSet rs=null;
            Statement st=null;
            PreparedStatement pst =null;

            @Override
            public void actionPerformed(ActionEvent e) {
                Select select = new Select();
                String num=textField_route.getText();
                //System.out.println(num);
                String sql = "select COUNT(*) from travel_information where route_num='"+num+"'";
                int exitNum = select.getCount(sql);
                if(exitNum<=0){
                    JOptionPane.showMessageDialog(null, "路线号错误！请返回重新输入！");
                }
                else {
                    columnNames = new Vector();
                    //设置列名
                    columnNames.add("路线号");
                    columnNames.add("旅游团编号");
                    columnNames.add("费用");
                    columnNames.add("详情介绍");
                    columnNames.add("旅游团名");
                    columnNames.add("导游姓名");
                    columnNames.add("地址");
                    columnNames.add("电话");
                    columnNames.add("出发地");
                    columnNames.add("目的地");
                    columnNames.add("天数");
                    columnNames.add("主要景点");
                    columnNames.add("管理（需管理员权限）");

                    //rowData可以存放多行,开始从数据库里取
                    rowData = new Vector();

                    String url = "jdbc:mysql://localhost:3306/case8?useUnicode=true&amp;characterEncoding=UTF-8";
                    String username = "root";
                    String password = "123456";
                    try {
                        Class.forName("com.mysql.jdbc.Driver");
                        conn = DriverManager.getConnection(url, username, password);
                    } catch (ClassNotFoundException cnfex) { //捕获加载驱动程序异常
                        System.err.println("装载 JDBC/ODBC 驱动程序失败。");
                        cnfex.printStackTrace();
                        System.exit(1); // terminate program
                    } catch (SQLException sqlex) { //捕获连接数据库异常
                        System.err.println("无法连接数据库");
                        sqlex.printStackTrace();
                        System.exit(1); // terminate program
                    }
                    try {

                        String sql1 = "select * from travel_information as t left join tourism_group as tg using (group_num) left join tourism_line as tl using (route_num) where t.route_num=?";
                        pst = conn.prepareStatement(sql1);
                        pst.setString(1, num);
                        rs = pst.executeQuery();

                        while (rs.next()) {
                            //rowData可以存放多行
                            Vector hang = new Vector();
                            hang.add(rs.getInt(1));//获取第一列
                            hang.add(rs.getInt(2));
                            hang.add(rs.getString(3));
                            hang.add(rs.getString(4));
                            hang.add(rs.getString(5));
                            hang.add(rs.getString(6));
                            hang.add(rs.getString(7));
                            hang.add(rs.getString(8));
                            hang.add(rs.getString(9));
                            hang.add(rs.getString(10));
                            hang.add(rs.getString(11));
                            hang.add(rs.getString(12));
                            //hang.add("");
                            //加入到rowData
                            rowData.add(hang);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    } finally {

                        try {
                            if (rs != null) {
                                rs.close();
                            }
                            if (st != null) {
                                st.close();
                            }
                            if (conn != null) {
                                conn.close();
                            }
                        } catch (SQLException se) {
                            se.printStackTrace();
                        }
                    }

                    //初始化Jtable
                    jt = new JTable(rowData, columnNames);
                    jt.setDefaultRenderer(Object.class, new format());//给表格设置渲染器
                    //jt.setEnabled(false);//内容不可更改
                    /*jt.setShowHorizontalLines(false);
                    jt.setShowVerticalLines(false);*/
                    //设置每列宽度
                    jt.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                    jt.getColumnModel().getColumn(0).setPreferredWidth(80);
                    jt.getColumnModel().getColumn(2).setPreferredWidth(95);
                    jt.getColumnModel().getColumn(3).setPreferredWidth(370);
                    jt.getColumnModel().getColumn(4).setPreferredWidth(140);
                    jt.getColumnModel().getColumn(5).setPreferredWidth(100);
                    jt.getColumnModel().getColumn(6).setPreferredWidth(100);
                    jt.getColumnModel().getColumn(7).setPreferredWidth(100);
                    jt.getColumnModel().getColumn(8).setPreferredWidth(100);
                    jt.getColumnModel().getColumn(9).setPreferredWidth(100);
                    jt.getColumnModel().getColumn(10).setPreferredWidth(95);
                    jt.getColumnModel().getColumn(11).setPreferredWidth(195);
                    jt.getColumnModel().getColumn(12).setPreferredWidth(195);
                    jt.setBackground(new Color(176, 211, 238, 207));

                    //在表格内添加修改按钮
                    jt.getColumnModel().getColumn(12).setCellEditor(new MyRender());//编辑器
                    jt.getColumnModel().getColumn(12).setCellRenderer(new MyRender());//渲染器

                    //表头
                    JTableHeader head =jt.getTableHeader();
                    head.setReorderingAllowed(false);//表头不可以动
                    head.setPreferredSize(new Dimension(jt.getColumnModel().getTotalColumnWidth(),40));
                    jt.getTableHeader().setFont(new Font(null, Font.BOLD, 14));  // 设置表头名称字体样式
                    head.setBackground(new Color(127, 155, 180));

                    //初始化 jsp
                    jsp = new JScrollPane(jt);
                    JFrame j = new JFrame("详细信息");
                    ///把jsp放入到jframe
                    j.add(jsp);
                    j.setBounds(0, 0, 1550, 600);
                    //j.setResizable(false);//让窗口大小不可改变
                    setLayout(null);//窗体大小固定，清空布局
                    j.setLocationRelativeTo(null);//让窗口在屏幕中间显示
                    j.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    j.setVisible(true);

                }
            }
        });
    }
}


class MyRender extends AbstractCellEditor implements TableCellRenderer, TableCellEditor {
    private static final long serialVersionUID = 1L;
    private JButton button = null;
    Select select=new Select();

    public MyRender() {
        //点击进入全部路线详情界面

        button = new JButton("点击管理信息");
        button.setFocusPainted(false);//去掉按钮周围的焦点框
        button.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String sql = "SELECT COUNT(*) FROM `user` WHERE user_state='已登录'";
                        int reselt = select.getCount(sql);
                        if (reselt>0) {
                            //判断当前登录的用户身份
                            String user_type = select.getString("SELECT user_type FROM `user` WHERE user_state='已登录'");
                            if (user_type.equals("管理员")) {
                                    travel_Management  t = new travel_Management();
                                     t.setVisible(true);
                                     //dispose();
                            }else{
                                JOptionPane.showMessageDialog(null, "当前无权限！请登录管理员账号！");
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, "请先登录！");
                            Login l = new Login();
                            l.setVisible(true);
                            //dispose();
                        }
                    }
                }
        );

    }
/*
    private void addWindowListener(WindowAdapter windowAdapter) {
    }*/

    @Override
    public Object getCellEditorValue() {
        // TODO Auto-generated method stub
        return null;
    }
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        // TODO Auto-generated method stub
        return button;
    }
    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        // TODO Auto-generated method stub
        return button;
    }
}